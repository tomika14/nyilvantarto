<?php
include ("functions/config.php");
include ("templates/dash-head.php");
$error = "";
$conn = new mysqli(HOST,USER,PASS,DB);
mysqli_set_charset($conn,"utf8");
$admin = $_SESSION["rights"];
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    $error = "Nem sikerült a kapcsolat.";
}
function sortorder($fieldname){
    $sorturl = "?order_by=".$fieldname."&sort=";
    $sorttype = "asc";
    if(isset($_GET['order_by']) && $_GET['order_by'] == $fieldname){
        if(isset($_GET['sort']) && $_GET['sort'] == "asc"){
            $sorttype = "desc";
        }
    }
    $sorturl .= $sorttype;
    return $sorturl;
}

$orderby = " ORDER BY id desc ";
if(isset($_GET['order_by']) && isset($_GET['sort'])){
    $orderby = ' order by '.$_GET['order_by'].' '.$_GET['sort'];
}
$limit = 50;
if (isset($_GET["page"])) {
    $page  = $_GET["page"];
}
else{
    $page=1;
};
$start_from = ($page-1) * $limit;
$sql = "SELECT szerzid,szerz_jel,partner_nev,szerzodes_targy,szerzodes_kelte,idoszak_kezd,idoszak_vege,millio,name,atlathato,pdf FROM szerzodesek INNER JOIN users ON szerzodesek.userid = users.id ".$orderby." LIMIT ".$start_from.", ".$limit."";

$result = $conn->query($sql);
if (!$result) {
    trigger_error('Invalid query: ' . $conn->error);
    $error = "Invalid Query!";
}
?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Szerződések</h1>

        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <form method="post" action="functions/export.php">
                <button type="submit" id="export" name="export" value="<?php echo $sql;?>" class="btn btn-sm btn-outline-secondary">Exportálás</button>
                </form>
            </div>
        </div>
    </div>
    <form action="kereses.php" method="POST">
        <div class="form-check">
            <select class="form-control" id="szerz_jel" name="keres_valaszt" onchange="search(this.value)">
                <option value="szerzid">Szerződés azonosító</option>
                <option value="szerz_jel">Szerződést kötő jele</option>
                <option value="szerz_tipus">Szerződés típus</option>
                <option value="szerz_irany">Szerződés irány</option>
                <option value="partner_nev">Partner neve</option>
                <option value="partner_tipus">Partner típus</option>
                <option value="szerzodes_targy">Szerződés tárgya</option>
                <option value="szerzodes_kelte">Szerződés kelte</option>
                <option value="idoszak_kezd">Vonatkozó időszak kezdete</option>
                <option value="idoszak_vege">Vonatkozó időszak vége</option>
                <option value="millio">5M forintot meghaladó</option>
                <option value="netto">Nettó összeg(Ft.)</option>
                <option value="brutto">Bruttó összeg(Ft.)</option>
                <option value="dijfiz_gyak">Díjfizetés gyakorisága</option>
                <option value="hatarozott">Határozott</option>
                <option value="asp_szerz">ASP szerződés szám</option>
                <option value="rovatrend">Rovatrend</option>
                <option value="cofog">COFOG</option>
                <option value="reszl_kod">Részletező kód</option>
            </select>
        </div>
        <div class="form-check" id="keres">
            <input type="text" class="form-control" id="keres_input" name="keres_text" placeholder="Keresés...">
        </div>
        <div class="form-check">
        <input type="submit" class="btn btn-primary" value="Keresés">
        </div>
    </form>
    <?php
    if (!empty($error)) {
    echo "<div class='alert alert-danger text-center role='alert'>";
    echo $error;
    echo "</div>";
    }

    if ($result->num_rows > 0) {
        echo "<div class='table-responsive' style='margin-top: 40px'>";
        echo "<table class='table table-striped table-sm text-center'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>";
        echo "<a href=".sortorder('szerzid').">Szerződés száma:</a>";
        echo "</th>";
        echo "<th>";
        echo "<a href=".sortorder('szerz_jel').">Szerződést kötő jele:</a>";
        echo "</th>";
        echo "<th>";
        echo "<a href=".sortorder('partner_nev').">Partner neve:</a>";
        echo "</th>";
        echo "<th>";
        echo "<a href=".sortorder('szerzodes_targy').">Szerződés tárgya:</a>";
        echo "</th>";
        echo "<th>";
        echo "<a href=".sortorder('szerzodes_kelte').">Szerződés kelte:</a>";
        echo "</th>";
        echo "<th>";
        echo "<a href=".sortorder('idoszak_kezd').">Vonatkozó időszak kezdete:</a>";
        echo "</th>";
        echo "<th>";
        echo "<a href=".sortorder('idoszak_vege').">Vonatkozó időszak vége:</a>";
        echo "</th>";
        echo "<th>";
        echo "<a href=".sortorder('millio').">5M forintot meghaladó:</a>";
        echo "</th>";
        echo "<th>";
        echo "<a href=".sortorder('name').">Készítette:</a>";
        echo "</th>";

        echo "<th>";
        echo "Megtekintés:";
        echo "</th>";
        if($admin < 2){
        echo "<th>";
        echo "Módosítás";
        echo "</th>";
        echo "<th>";
        echo "Törlés";
        echo "</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        }
    while($row = $result->fetch_assoc()) {

            echo "<tr>";
        echo "<td>";
        echo "$row[szerzid]";
        echo "</td>";
            echo "<td>";
            if($row["szerz_jel"] == 0){
                echo "ÖNK";
            }
            elseif($row["szerz_jel"]==1){
                echo "HIV";
            }
            elseif ($row["szerz_jel"] == 2){
                echo "NNÖ";
            }
                echo "</td>";
            echo "<td>";
                echo "$row[partner_nev]";
                echo "</td>";
            echo "<td>";
                echo "$row[szerzodes_targy]";
                echo "</td>";
            echo "<td>";
                echo "$row[szerzodes_kelte]";
                echo "</td>";
                            echo "<td>";
                echo "$row[idoszak_kezd]";
                echo "</td>";
                            echo "<td>";
                echo "$row[idoszak_vege]";
                echo "</td>";
        echo "<td>";
        if($row["millio"] == 0){
            echo "Igen";
        }
        elseif($row["millio"] == 1){
            echo "Nem";
        }
        echo "</td>";

                echo "<td>";
                echo "$row[name]";
                echo "</td>";

                echo "<td>";
                echo "<a href='szerz.php?id=$row[szerzid]'>Megtekintés</a>";
                echo "</td>";
        if($admin < 2){
            echo "<td>";
                echo "<a href='modszerz.php?id=$row[szerzid]'>Módosítás</a>";
                echo "</td>";
            echo "<td>";
                echo "<a href='delszerz.php?id=$row[szerzid]' onclick='return confirm(\"Biztos, hogy törli?\")'>Törlés</a>";
                echo "</td>";
            echo "</tr>";
        }


    }
    echo "</tbody>";
        echo "</table>";
    } else {
        echo "Nincs szerződés!";
        $error = "Nincs szerződés!";
    }
    $result_db = mysqli_query($conn,"SELECT COUNT(szerzid) FROM szerzodesek");
    $row_db = mysqli_fetch_row($result_db);
    $total_records = $row_db[0];
    $total_pages = ceil($total_records / $limit);
    /* echo  $total_pages; */
    $pagLink = "<ul class='pagination'>";
    $sortby = $_GET["order_by"];
    $sor = $_GET["sort"];
    for ($i=1; $i<=$total_pages; $i++) {
        $pagLink .= "<li class='page-item'><a class='page-link' href='szerzodesek.php?page=".$i."&order_by=".$sortby."&sort=".$sor."'>".$i."</a></li>";
    }
    echo $pagLink . "</ul>";

    $conn->close();

    ?>
        <?php
include ("templates/dash-foot.php");
?>
