<?php
include ("templates/dash-head.php");
include ("admin_auth.php");
$id = $_SESSION["id"];
use Phppot\DataSource;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
//use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

require_once 'DataSource.php';
$db = new DataSource();
$conn = $db->getConnection();
require_once ('vendor/autoload.php');

if (isset($_POST["import"])) {

    $allowedFileType = [
        'application/vnd.ms-excel',
        'text/xls',
        'text/xlsx',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    ];

    if (in_array($_FILES["file"]["type"], $allowedFileType)) {

        $targetPath = 'upload/' . $_FILES['file']['name'];
        move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);

        $Reader = new PhpOffice\PhpSpreadsheet\Reader\Xlsx();


        $spreadSheet = $Reader->load($targetPath);
        $excelSheet = $spreadSheet->getActiveSheet();
        $spreadSheetAry = $excelSheet->toArray();
        $sheetCount = count($spreadSheetAry);

        for ($i = 1; $i <= $sheetCount; $i ++) {
            $szerz_jel = "";
            if (isset($spreadSheetAry[$i][0])) {
                $szerz_jel = mysqli_real_escape_string($conn, $spreadSheetAry[$i][0]);
            }
            $partner_nev = "";
            if (isset($spreadSheetAry[$i][1])) {
                $partner_nev = mysqli_real_escape_string($conn, $spreadSheetAry[$i][1]);
            }
            $szerzodes_targy = "";
            if (isset($spreadSheetAry[$i][1])) {
                $szerzodes_targy = mysqli_real_escape_string($conn, $spreadSheetAry[$i][2]);
            }
            $szerzodes_kelte = "";
            if (isset($spreadSheetAry[$i][1])) {
                $szerzodes_kelte = mysqli_real_escape_string($conn, $spreadSheetAry[$i][3]);
            }
            $idoszak_kezd = "";
            if (isset($spreadSheetAry[$i][1])) {
                $idoszak_kezd = mysqli_real_escape_string($conn, $spreadSheetAry[$i][4]);
            }
            $idoszak_vege = "";
            if (isset($spreadSheetAry[$i][1])) {
                $idoszak_vege = mysqli_real_escape_string($conn, $spreadSheetAry[$i][5]);
            }
            $netto = "";
            if (isset($spreadSheetAry[$i][1])) {
                $netto = mysqli_real_escape_string($conn, $spreadSheetAry[$i][6]);
            }
            $brutto = "";
            if (isset($spreadSheetAry[$i][1])) {
                $brutto = mysqli_real_escape_string($conn, $spreadSheetAry[$i][7]);
            }
            $dijfiz_gyak = "";
            if (isset($spreadSheetAry[$i][1])) {
                $dijfiz_gyak = mysqli_real_escape_string($conn, $spreadSheetAry[$i][8]);
            }
            $megjegyzes = "";
            if (isset($spreadSheetAry[$i][1])) {
                $megjegyzes = mysqli_real_escape_string($conn, $spreadSheetAry[$i][9]);
            }
            $hatarozott = "";
            if (isset($spreadSheetAry[$i][1])) {
                $hatarozott = mysqli_real_escape_string($conn, $spreadSheetAry[$i][16]);
            }
            $millio = "";
            if (isset($spreadSheetAry[$i][1])) {
                $millio = mysqli_real_escape_string($conn, $spreadSheetAry[$i][11]);
            }
            $asp_szerz = "";
            if (isset($spreadSheetAry[$i][1])) {
                $asp_szerz = mysqli_real_escape_string($conn, $spreadSheetAry[$i][12]);
            }
            $rovatrend = "";
            if (isset($spreadSheetAry[$i][1])) {
                $rovatrend = mysqli_real_escape_string($conn, $spreadSheetAry[$i][13]);
            }
            $cofog = "";
            if (isset($spreadSheetAry[$i][1])) {
                $cofog = mysqli_real_escape_string($conn, $spreadSheetAry[$i][14]);
            }
            $reszl_kod = "";
            if (isset($spreadSheetAry[$i][1])) {
                $reszl_kod = mysqli_real_escape_string($conn, $spreadSheetAry[$i][15]);
            }
            $userid = $id;

            if (! empty($szerz_jel) || ! empty($partner_nev)) {
                $query = "insert into szerzodesek (szerz_jel,partner_nev,szerzodes_targy,szerzodes_kelte,idoszak_kezd,idoszak_vege,netto,brutto,dijfiz_gyak,megjegyzes,hatarozott,millio,asp_szerz,rovatrend,cofog,reszl_kod,userid) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                $paramType = "sssssssssssssssss";
                $paramArray = array(
                    $szerz_jel,
                    $partner_nev,
                    $szerzodes_targy,
                    $szerzodes_kelte,
                    $idoszak_kezd,
                    $idoszak_vege,
                    $netto,
                    $brutto,
                    $dijfiz_gyak,
                    $megjegyzes,
                    $hatarozott,
                    $millio,
                    $asp_szerz,
                    $rovatrend,
                    $cofog,
                    $reszl_kod,
                    $userid
                );
                $insertId = $db->insert($query, $paramType, $paramArray);
                // $query = "insert into tbl_info(name,description) values('" . $name . "','" . $description . "')";
                // $result = mysqli_query($conn, $query);

                if (! empty($insertId)) {
                    $type = "success";
                    $message = "Excel Data Imported into the Database";
                } else {
                    $type = "error";
                    $message = "Problem in Importing Excel Data";
                }
            }
        }
    } else {
        $type = "error";
        $message = "Invalid File Type. Upload Excel File.";
    }
}

?>


<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Excel fájl importálása</h1>

    </div>
    <div id="response"
         class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>

    <form action="" method="post" name="frmExcelImport"
          id="frmExcelImport" enctype="multipart/form-data">
        <div>

            <input type="file" name="file" id="file" accept=".xls,.xlsx"><br />
            <button type="submit" id="submit" name="import"
                    class="btn btn-primary mt-3">Import</button>

        </div>

    </form>
<?php
include ("templates/dash-foot.php");
?>