<?php
include ("auth.php");
?>
<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>Nyílvántartó</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/dashboard/">

    <!-- Bootstrap core CSS -->
    <link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- choose a theme file -->
    <!-- load jQuery and tablesorter scripts -->
    <script type="text/javascript" src="js/search.js"></script>


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3 text-uppercase" href="dash.php">Szerződés nyílvántartó</a>

    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <!--<input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">-->
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="https://pilisvorosvar.hu">PILISVÖRÖSVÁR.HU</a>
        </li>
    </ul>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="sidebar-sticky pt-3 pre-scrollable mh-100">
                <ul class="nav flex-column">
                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                        <span>Felhasználói adatok</span>
                    </h6>
                    <li class="nav-item">
                        <span class="nav-link">
                        <span data-feather="user"></span>
                            Név: <?php echo $_SESSION['name']; ?>
                        </span>
                    </li>
                    <li class="nav-item">
                        <span class="nav-link">
                            <span data-feather="mail"></span>
                            Email cím: <?php echo $_SESSION['email']; ?>
                        </span>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="logout.php">
                            <span data-feather="log-out"></span>
                            Kijelentkezés
                        </a>
                    </li>
                </ul>

                <ul class="nav flex-column mb-2">
                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                        <span>Szerződések</span>
                    </h6>
                    <li class="nav-item">
                        <a class="nav-link" href="dash.php">
                            <span data-feather="home"></span>
                            Kezdőlap
                        </a>
                    </li>
                    <?php
                    $admin = $_SESSION["rights"];
                    if($admin < 2){
                        ?>
                    <li class="nav-item">
                        <a class="nav-link" href="ujszerzodes.php">
                            <span data-feather="file-plus"></span>
                            Új szerződés
                        </a>
                    </li>
                    <?php
                    }
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="szerzodesek.php?page=1&order_by=szerzid&sort=asc">
                            <span data-feather="file-text"></span>
                            Szerződések
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="felhasznaloi_beallitasok.php">
                            <span data-feather="settings"></span>
                            Felhasználói beállítások
                        </a>
                    </li>
                </ul>

                <?php
                if($admin === 1){
                ?>
                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <span>Admin felület</span>
                </h6>
                <ul class="nav flex-column mb-2">
                    <li class="nav-item">
                        <a class="nav-link" href="felhasznalo_hozzaad.php">
                            <span data-feather="user-plus"></span>
                            Felhasználó hozzáadása
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="felhasznalo_lista.php">
                            <span data-feather="users"></span>
                            Felhasználók listája
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="import.php">
                            <span data-feather="upload-cloud"></span>
                            Importálás
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="logs.php?page=1&order_by=log_id&sort=asc">
                            <span data-feather="clipboard"></span>
                            Log
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="settings.php">
                            <span data-feather="settings"></span>
                            Beállítások
                        </a>
                    </li>
                </ul>
                <?php
                }
                ?>
                <!-- Copyright -->
                <div class="footer-copyright text-center py-3 fixed">© 2021
                    <a href="https://pilisvorosvar.hu/" class="text-muted">Pilisvörösvár</a>
                </div>
                <!-- Copyright -->
            </div>
        </nav>
    </div>
</div>
