<?php
include ("templates/dash-head.php");
require_once ("functions/config.php");
include ("admin_auth.php.php");
$id = $_GET["id"];
$conn = new mysqli(HOST,USER,PASS,DB);
mysqli_set_charset($conn,"utf8");
if ($conn->connect_error) {
die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT id,username,email,name,rights,create_date FROM users WHERE id = '$id'";
$result = $conn->query($sql);
if (!$result) {
trigger_error('Invalid query: ' . $conn->error);
}
if ($result->num_rows > 0) {
while($row = $result->fetch_assoc()) {

?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php echo "$row[username]"; ?> nevű felhasználó adatai:</h1>
    </div>
    <form>
        <div class="form-group">
            <label for="id">Azonosító szám:</label>
            <input type="text" class="form-control" id="id" value="<?php echo "$row[id]";?>" readonly>
            <label for="username">Felhasználónév:</label>
            <input type="text" class="form-control" id="username" value="<?php echo "$row[username]"; ?>" readonly>
            <label for="email">E-mail cím:</label>
            <input type="text" class="form-control" id="email" value="<?php echo "$row[email]"; ?>" readonly>
            <label for="name">Név:</label>
            <input type="text" class="form-control" id="name" value="<?php echo "$row[name]"; ?>" readonly>
            <label for="rights">Jogosultság:</label>
            <input type="text" class="form-control" id="rights" value="<?php
            if($row["rights"] == 0){
                echo "Felhasználó";
            }elseif($row["rights"] == 1){
                echo "Adminisztrátor";
            }
            ?>
" readonly>
            <label for="create_date">Létrehozás dátuma:</label>
            <input type="text" class="form-control" id="create_date" value="<?php echo "$row[create_date]"; ?>" readonly>
        </div>
    </form>

    <?php
}
}
include ("templates/dash-foot.php");
?>
