<?php
include("functions/config.php");
include("templates/dash-head.php");
if ($_SERVER["REQUEST_METHOD"] == "POST"){
$keres_valaszt = $_POST["keres_valaszt"];
$keres_text = $_POST["keres_text"];
$conn = new mysqli(HOST, USER, PASS, DB);
mysqli_set_charset($conn,"utf8");
$admin = $_SESSION["rights"];
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$sql = "SELECT szerzid,szerz_jel,partner_nev,szerzodes_targy,szerzodes_kelte,idoszak_kezd,idoszak_vege,millio,name,atlathato,pdf FROM szerzodesek INNER JOIN users ON szerzodesek.userid = users.id WHERE ".$keres_valaszt." LIKE '%$keres_text%' LIMIT 0, 5000";
$result = $conn->query($sql);
if (!$result) {
    trigger_error('Invalid query: ' . $conn->error);
}


?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Szerződések</h1>

        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <form method="post" action="functions/export.php">
                    <button type="submit" id="export" name="export" value="<?php echo $sql;?>" class="btn btn-sm btn-outline-secondary">Exportálás</button>
                </form>
            </div>
        </div>
    </div>
    <form action="kereses.php" method="POST">
        <div class="form-check">
            <select class="form-control" id="szerz_jel" name="keres_valaszt" onchange="search(this.value)">
                <option value="szerzid">Szerződés azonosító</option>
                <option value="szerz_jel">Szerződést kötő jele</option>
                <option value="szerz_tipus">Szerződés típus</option>
                <option value="szerz_irany">Szerződés irány</option>
                <option value="partner_nev">Partner neve</option>
                <option value="partner_tipus">Partner típus</option>
                <option value="szerzodes_targy">Szerződés tárgya</option>
                <option value="szerzodes_kelte">Szerződés kelte</option>
                <option value="idoszak_kezd">Vonatkozó időszak kezdete</option>
                <option value="idoszak_vege">Vonatkozó időszak vége</option>
                <option value="millio">5M forintot meghaladó</option>
                <option value="netto">Nettó összeg(Ft.)</option>
                <option value="brutto">Bruttó összeg(Ft.)</option>
                <option value="dijfiz_gyak">Díjfizetés gyakorisága</option>
                <option value="hatarozott">Határozott</option>
                <option value="asp_szerz">ASP szerződés szám</option>
                <option value="rovatrend">Rovatrend</option>
                <option value="cofog">COFOG</option>
                <option value="reszl_kod">Részletező kód</option>
            </select>
        </div>
        <div class="form-check" id="keres">
            <input type="text" class="form-control" id="keres_input" name="keres_text" placeholder="Keresés...">
        </div>
        <div class="form-check">
        <input type="submit" class="btn btn-primary" value="Keresés">
        </div>
    </form>
    <?php
    if ($result->num_rows > 0) {
        echo "<div class='table-responsive' style='margin-top: 40px'>";
        echo "<table class='table table-striped table-sm text-center'>";
        echo "<tr>";
        echo "<th>";
        echo "Szerződést kötő jele:";
        echo "</th>";
        echo "<th>";
        echo "Szerződés száma:";
        echo "</th>";
        echo "<th>";
        echo "Partner neve:";
        echo "</th>";
        echo "<th>";
        echo "Szerződés tárgya:";
        echo "</th>";
        echo "<th>";
        echo "Szerződés kelte:";
        echo "</th>";
        echo "<th>";
        echo "Vonatkozó időszak kezdete:";
        echo "</th>";
        echo "<th>";
        echo "Vonatkozó időszak vége:";
        echo "</th>";
        echo "<th>";
        echo "5M forintot meghaladó:";
        echo "</th>";
        echo "<th>";
        echo "Készítette:";
        echo "</th>";
        echo "<th>";
        echo "Megtekintés:";
        echo "</th>";
        if($admin < 2){
            echo "<th>";
            echo "Módosítás";
            echo "</th>";
            echo "<th>";
            echo "Törlés";
            echo "</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
        }
        while($row = $result->fetch_assoc()) {

            echo "<tr>";
            echo "<td>";
            echo "$row[szerzid]";
            echo "</td>";
            echo "<td>";
            if($row["szerz_jel"] == 0){
                echo "ÖNK";
            }
            elseif($row["szerz_jel"]==1){
                echo "HIV";
            }
            elseif ($row["szerz_jel"] == 2){
                echo "NNÖ";
            }
            echo "</td>";
            echo "<td>";
            echo "$row[partner_nev]";
            echo "</td>";
            echo "<td>";
            echo "$row[szerzodes_targy]";
            echo "</td>";
            echo "<td>";
            echo "$row[szerzodes_kelte]";
            echo "</td>";
            echo "<td>";
            echo "$row[idoszak_kezd]";
            echo "</td>";
            echo "<td>";
            echo "$row[idoszak_vege]";
            echo "</td>";
            echo "<td>";
            if($row["millio"] == 0){
                echo "Igen";
            }
            elseif($row["millio"]==1){
                echo "Nem";
            }
            echo "</td>";
            echo "<td>";
            echo "$row[name]";
            echo "</td>";

            echo "<td>";
            echo "<a href='szerz.php?id=$row[szerzid]'>Megtekintés</a>";
            echo "</td>";
            if($admin < 2){
                echo "<td>";
                echo "<a href='modszerz.php?id=$row[szerzid]'>Módosítás</a>";
                echo "</td>";
                echo "<td>";
                echo "<a href='delszerz.php?id=$row[szerzid]' onclick='return confirm(\"Biztos, hogy törli?\")'>Törlés</a>";
                echo "</td>";
                echo "</tr>";
            }


        }
        echo "</table>";
    } else {
        echo "Nincs szerződés!";
    }
    $conn->close();
   }
    ?>
    <?php
    include ("templates/dash-foot.php");
    ?>
