<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require '../vendor/phpmailer/phpmailer/src/Exception.php';
require '../vendor/phpmailer/phpmailer/src/PHPMailer.php';
require '../vendor/phpmailer/phpmailer/src/SMTP.php';
require '../vendor/autoload.php';
require_once "config.php";

$conn = new mysqli(HOST,USER,PASS,DB);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT smtp_add, smtp_port, smtp_name, smtp_pass, send_mail FROM settings";
$result = $conn->query($sql);
if (!$result) {
    trigger_error('Invalid query: ' . $conn->error);
}
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
//smtp_add, smtp_port, smtp_name, smtp_pass

//Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = SMTP::DEBUG_SERVER;      //Enable verbose debug output
            $mail->CharSet = 'UTF-8';
            $mail->isSMTP();                                            //Send using SMTP
            $mail->Host = $row["smtp_add"];                     //Set the SMTP server to send through
            $mail->SMTPAuth = true;                                   //Enable SMTP authentication
            $mail->Username = $row["smtp_name"];                     //SMTP username
            $mail->Password = $row["smtp_pass"];                               //SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port = $row["smtp_port"];                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
//rendszergazda@pilisvorosvar.hu
            //Recipients
            $mail->setFrom($row["smtp_name"], 'NYILVANTARTO');
            $mail->addAddress($row["send_mail"]);     //Add a recipient
            //$mail->addAddress('ellen@example.com');               //Name is optional
            //$mail->addReplyTo('info@example.com', 'Information');
            //$mail->addCC('cc@example.com');
            //$mail->addBCC('bcc@example.com');

            //Attachments
            //$mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

            $sql_2 = "SELECT szerzid, szerz_jel, partner_nev,szerzodes_targy, szerzodes_kelte, jelzes FROM szerzodesek WHERE timestampdiff(day,lejar,NOW()) <= 30 AND jelzes = 0";
            $result2 = $conn->query($sql_2);
            if (!$result2) {
                trigger_error('Invalid query: ' . $conn->error);
            }
            if ($result2->num_rows > 0) {
                while($rows = $result2->fetch_assoc()) {
                    $szerz_jel = "";
                    if($row["szerz_jel"] == 0){
                        $szerz_jel = "ÖNK";
                    }
                    elseif($row["szerz_jel"] == 1){
                        $szerz_jel = "HIV";
                    }
                    elseif($row["szerz_jel"] == 2){
                        $szerz_jel = "NNÖ";
                    }
                    //Content
                    $mail->isHTML(true);                                  //Set email format to HTML
                    $mail->Subject = 'Szerződés lejár!';
                    $mail->Body = 'Lejáró szerződés: <b>'.$rows["szerzid"].'</b><br />JEL: <b>'.$szerz_jel.'</b><br />PARTNER NEVE: <b>'.$rows["partner_nev"].'</b><br />SZERZŐDÉS TÁRGYA:<b>'.$rows["szerzodes_targy"].'</b><br />SZERZŐDÉS KELTE: <b>'.$rows["szerzodes_kelte"].'</b><br />';
                    //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
                    $mail->send();

                    $sql_3 = "UPDATE szerzodesek SET jelzes = 1 WHERE szerzid = '".$rows["szerzid"]."'";
                    if ($conn->query($sql_3) === TRUE) {
                        echo "Jelzes kikuldve.";
                    } else {
                        echo "Hiba. " . $conn->error;
                    }

                    $conn->close();
                }
            }
            //echo 'Message has been sent';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
}
?>