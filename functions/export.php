<?php
require_once "../vendor/autoload.php";
require_once "config.php";

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
if(isset($_POST["export"]))
{
    $exp = $_POST["export"];
    $spreadsheet = new Spreadsheet();
    $Excel_writer = new Xlsx($spreadsheet);

    $styleArrayFirstRow = [
        'font' => [
            'bold' => true,
        ]
    ];

$spreadsheet->setActiveSheetIndex(0);
$activeSheet = $spreadsheet->getActiveSheet();

$activeSheet->setCellValue('A1', 'Szerződést kötő jele:');
$activeSheet->setCellValue('B1', 'Szerződés száma:');
$activeSheet->setCellValue('C1', 'Partner neve:');
$activeSheet->setCellValue('D1', 'Szerződés tárgya:');
$activeSheet->setCellValue('E1', 'Szerződés kelte:');
$activeSheet->setCellValue('F1', 'Vonatkozó időszak kezdete:');
$activeSheet->setCellValue('G1', 'Vonatkozó időszak vége:');
$activeSheet->setCellValue('H1', '5M forintot meghaladó:');
$activeSheet->setCellValue('I1', 'Készítette:');
$activeSheet->setCellValue('J1', 'Átláthatósági nyilatkozat:');
$activeSheet->setCellValue('K1', 'Szerződés:');


$conn = mysqli_connect(HOST, USER, PASS, DB);
mysqli_set_charset($conn,'utf8');
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


    $query = $conn->query($exp);

    if ($query->num_rows > 0) {
        $i = 2;
        $szerz = "";
        $mill = "";
        while ($row = $query->fetch_assoc()) {
            if($row["szerz_jel"] == 0){
                $szerz = "ÖNK";
            }
            elseif($row["szerz_jel"]==1){
                $szerz = "HIV";
            }
            elseif ($row["szerz_jel"] == 2){
                $szerz = "NNÖ";
            }
            if($row["millio"] == 0){
                $mill = "Igen";
            }
            elseif($row["millio"]==1){
                $mill = "Nem";
            }

            $activeSheet->setCellValue('A' . $i, $szerz);
            $activeSheet->setCellValue('B' . $i, $row['szerzid']);
            $activeSheet->setCellValue('C' . $i, $row['partner_nev']);
            $activeSheet->setCellValue('D' . $i, $row['szerzodes_targy']);
            $activeSheet->setCellValue('E' . $i, $row['szerzodes_kelte']);
            $activeSheet->setCellValue('F' . $i, $row['idoszak_kezd']);
            $activeSheet->setCellValue('G' . $i, $row['idoszak_vege']);
            $activeSheet->setCellValue('H' . $i, $mill);
            $activeSheet->setCellValue('I' . $i, $row['name']);
            $activeSheet->setCellValue('J' . $i, $row['atlathato']);
            $activeSheet->setCellValue('K' . $i, $row['pdf']);
            $activeSheet->getCell('J'.$i)->getHyperlink()->setUrl("http://mavu.hu/nyilvantarto/upload/atlathatosagi/".$row['atlathato']);
            $activeSheet->getCell('K'.$i)->getHyperlink()->setUrl("http://mavu.hu/nyilvantarto/upload/szerzodes/".$row['pdf']);

            $i++;
        }
    }
    foreach(range('A','K') as $columnID) {
        $activeSheet->getColumnDimension($columnID)
            ->setAutoSize(true);
    }
    $styleArray = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ]];
    $spreadsheet->getActiveSheet()->getStyle('A1:K1')->applyFromArray($styleArray);
    $spreadsheet->getActiveSheet()->getStyle("A1:K1")->getFont()->setSize(14);
    $spreadsheet->getActiveSheet()->getStyle('A:K')->getAlignment()->setHorizontal('center');

    $filename = "export_". date("Y-m-d").".xlsx";

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename=' . $filename);
    header('Cache-Control: max-age=0');
    $Excel_writer->save('php://output');
}
?>