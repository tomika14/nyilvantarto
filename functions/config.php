<?php
define("HOST", "");
define("USER", "");
define("PASS", "");
define("DB", "");

$conn = mysqli_connect(HOST, USER, PASS, DB);
mysqli_set_charset($conn,'utf8');
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT smtp_add,smtp_port,smtp_name,smtp_pass FROM settings";

$result = $conn->query($sql);

if ($result->num_rows > 0)
{
    while($row = $result->fetch_assoc()) {
        define("SMTP_ADDRESS",$row["smtp_add"]);
        define("SMTP_PORT", $row["smtp_port"]);
        define("SMTP_NAME", $row["smtp_name"]);
        define("SMTP_PASS", $row["smtp_pass"]);
    }
}

?>
