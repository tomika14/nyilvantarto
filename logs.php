<?php
include("templates/dash-head.php");
include ("admin_auth.php");
require_once ("functions/config.php");
?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Log</h1>

    </div>
    <?php
    $conn = new mysqli(HOST,USER,PASS,DB);
    mysqli_set_charset($conn,"utf8");
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    function sortorder($fieldname){
        $sorturl = "?order_by=".$fieldname."&sort=";
        $sorttype = "asc";
        if(isset($_GET['order_by']) && $_GET['order_by'] == $fieldname){
            if(isset($_GET['sort']) && $_GET['sort'] == "asc"){
                $sorttype = "desc";
            }
        }
        $sorturl .= $sorttype;
        return $sorturl;
    }

    $orderby = " ORDER BY id desc ";
    if(isset($_GET['order_by']) && isset($_GET['sort'])){
        $orderby = ' order by '.$_GET['order_by'].' '.$_GET['sort'];
    }
    $limit = 50;
    if (isset($_GET["page"])) {
        $page  = $_GET["page"];
    }
    else{
        $page=1;
    };
    $start_from = ($page-1) * $limit;


    $sql = "SELECT log_id, log_text, log_user, log_date, log_szerzid,name FROM naplozas INNER JOIN users ON users.id = naplozas.log_user  ".$orderby." LIMIT ".$start_from.", ".$limit."";
    $result = $conn->query($sql);
    if (!$result) {
        trigger_error('Invalid query: ' . $conn->error);
    }
    if ($result->num_rows > 0) {
        echo "<div class='table-responsive'>";
        echo "<table class='table table-striped table-sm text-center'>";
        echo "<tr>";
        echo "<th>";
        echo "<a href=".sortorder('log_id').">LOG Azonosító:</a>";
        echo "</th>";
        echo "<th>";
        echo "<a href=".sortorder('log_text').">Szöveg:</a>";
        echo "</th>";
        echo "<th>";
        echo "<a href=".sortorder('log_user').">Felhasználó azonosító</a>";
        echo "</th>";
        echo "<th>";
        echo "<a href=".sortorder('name').">Felhasználó neve:</a>";
        echo "</th>";
        echo "<th>";
        echo "<a href=".sortorder('log_szerzid').">Szerződés azonosító:</a>";
        echo "</th>";
        echo "<th>";
        echo "<a href=".sortorder('log_date').">Dátum:</a>";
        echo "</th>";
        while($row = $result->fetch_assoc()) {

            echo "<tr>";
            echo "<td>";
            echo "$row[log_id]";
            echo "</td>";
            echo "<td>";
            echo "$row[log_text]";
            echo "</td>";
            echo "<td>";
            echo "$row[log_user]";
            echo "</td>";
            echo "<td>";
            echo "$row[name]";
            echo "</td>";
            echo "<td>";
            echo "$row[log_szerzid]";
            echo "</td>";
            echo "<td>";
            echo "$row[log_date]";
            echo "</td>";
            echo "</tr>";


        }
        echo "</table>";
    } else {
        echo "Nincs log!";
    }
    $result_db = mysqli_query($conn,"SELECT COUNT(log_id) FROM naplozas");
    $row_db = mysqli_fetch_row($result_db);
    $total_records = $row_db[0];
    $total_pages = ceil($total_records / $limit);
    /* echo  $total_pages; */
    $pagLink = "<ul class='pagination'>";
    $sortby = $_GET["order_by"];
    $sor = $_GET["sort"];
    for ($i=1; $i<=$total_pages; $i++) {
        $pagLink .= "<li class='page-item'><a class='page-link' href='logs.php?page=".$i."&order_by=".$sortby."&sort=".$sor."'>".$i."</a></li>";
    }
    echo $pagLink . "</ul>";


    $conn->close();
    ?>
    <?php
    include ("templates/dash-foot.php");
    ?>
