<?php
include("templates/dash-head.php");
include ("functions/config.php");
include ("admin_auth.php.php");

$id = $_GET["id"];
$success = "";
$error = "";

$conn = new mysqli(HOST, USER, PASS, DB);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// sql to delete a record
$sql = "DELETE FROM users WHERE id='$id'";

if ($conn->query($sql) === TRUE) {
    $success = "Sikeres törlés!";
} else {
    $error = "Hiba! " . $conn->error;
}

$conn->close();
?>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Törlés</h1>

        </div>
<?php
if (!empty($success)) {
    echo "<div class='alert alert-success text-center role='alert'>";
    echo $success;
    echo "</div>";
}
if (!empty($error)) {
    echo "<div class='alert alert-danger text-center role='alert'>";
    echo $error;
    echo "</div>";
}
include ("templates/dash-foot.php");
?>