function search(val){
    if(val==="hatarozott") {
        /* Többi tag törlése */
        if(document.getElementById("keres_input") !== null){
            document.getElementById("keres_input").remove();
        }
        if(document.getElementById("sel_irany") !== null){
            document.getElementById("sel_irany").remove();
        }
        if(document.getElementById("sel_kot") !== null){
            document.getElementById("sel_kot").remove();
        }
        if(document.getElementById("sel_millio") !== null){
            document.getElementById("sel_millio").remove();
        }
        if(document.getElementById("sel_partner") !== null){
            document.getElementById("sel_partner").remove();
        }
        if(document.getElementById("sel_szerztip") !== null){
            document.getElementById("sel_szerztip").remove();
        }

        /* Hozzáadás */
        var sel_hat = document.createElement("SELECT");
        sel_hat.setAttribute("id","sel_hat");
        sel_hat.setAttribute("class","form-control");
        sel_hat.setAttribute("name","keres_text");
        document.getElementById("keres").appendChild(sel_millio);

        /*Opt1*/
        var opt1_hat = document.createElement("OPTION");
        opt1_hat.setAttribute("value","0");
        var opt1_hat_szoveg = document.createTextNode("Igen");
        opt1_hat.appendChild(opt1_millio_szoveg);

        /*Opt2*/
        var opt2_hat = document.createElement("OPTION");
        opt2_hat.setAttribute("value","1");
        var opt2_hat_szoveg = document.createTextNode("Nem");
        opt2_hat.appendChild(opt2_hat_szoveg);

        /* SELECT elemhez való hozzáadás */
        document.getElementById("sel_hat").appendChild(opt1_hat);
        document.getElementById("sel_hat").appendChild(opt2_hat);
    }
    else if(val==="szerz_jel")
    {
        /* Többi tag törlése */
        if(document.getElementById("keres_input") !== null){
            document.getElementById("keres_input").remove();}

        if(document.getElementById("sel_irany") !== null){
            document.getElementById("sel_irany").remove();
        }
        if(document.getElementById("sel_millio") !== null){
            document.getElementById("sel_millio").remove();
        }
        if(document.getElementById("sel_hat") !== null){
            document.getElementById("sel_hat").remove();
        }
        if(document.getElementById("sel_partner") !== null){
            document.getElementById("sel_partner").remove();
        }
        if(document.getElementById("sel_szerztip") !== null){
            document.getElementById("sel_szerztip").remove();
        }

        /* Hozzáadás */
        var sel_irany = document.createElement("SELECT");
        sel_irany.setAttribute("id","sel_kot");
        sel_irany.setAttribute("class","form-control");
        sel_irany.setAttribute("name","keres_text");
        document.getElementById("keres").appendChild(sel_irany);

        /*Opt1*/
        var opt1_kot = document.createElement("OPTION");
        opt1_kot.setAttribute("value","0");
        var opt1_kot_szoveg = document.createTextNode("ÖNK");
        opt1_kot.appendChild(opt1_kot_szoveg);

        /*Opt2*/
        var opt2_kot = document.createElement("OPTION");
        opt2_kot.setAttribute("value","1");
        var opt2_kot_szoveg = document.createTextNode("HIV");
        opt2_kot.appendChild(opt2_kot_szoveg);

        /*Opt3*/
        var opt3_kot = document.createElement("OPTION");
        opt3_kot.setAttribute("value","2");
        var opt3_kot_szoveg = document.createTextNode("NNÖ");
        opt3_kot.appendChild(opt3_kot_szoveg);

        /* SELECT elemhez való hozzáadás */
        document.getElementById("sel_kot").appendChild(opt1_kot);
        document.getElementById("sel_kot").appendChild(opt2_kot);
        document.getElementById("sel_kot").appendChild(opt3_kot);


    }
    else if(val==="millio")
    {
        /* Többi tag törlése */
        if(document.getElementById("keres_input") !== null){
            document.getElementById("keres_input").remove();
        }
        if(document.getElementById("sel_irany") !== null){
            document.getElementById("sel_irany").remove();
        }
        if(document.getElementById("sel_kot") !== null){
            document.getElementById("sel_kot").remove();
        }
        if(document.getElementById("sel_hat") !== null){
            document.getElementById("sel_hat").remove();
        }
        if(document.getElementById("sel_partner") !== null){
            document.getElementById("sel_partner").remove();
        }
        if(document.getElementById("sel_szerztip") !== null){
            document.getElementById("sel_szerztip").remove();
        }
        /* Hozzáadás */
        var sel_millio = document.createElement("SELECT");
        sel_millio.setAttribute("id","sel_millio");
        sel_millio.setAttribute("class","form-control");
        sel_millio.setAttribute("name","keres_text");
        document.getElementById("keres").appendChild(sel_millio);

        /*Opt1*/
        var opt1_millio = document.createElement("OPTION");
        opt1_millio.setAttribute("value","0");
        var opt1_millio_szoveg = document.createTextNode("Igen");
        opt1_millio.appendChild(opt1_millio_szoveg);

        /*Opt2*/
        var opt2_millio = document.createElement("OPTION");
        opt2_millio.setAttribute("value","1");
        var opt2_millio_szoveg = document.createTextNode("Nem");
        opt2_millio.appendChild(opt2_millio_szoveg);

        /* SELECT elemhez való hozzáadás */
        document.getElementById("sel_millio").appendChild(opt1_millio);
        document.getElementById("sel_millio").appendChild(opt2_millio);

    }
    else if(val==="szerz_irany")
    {
        /* Többi tag törlése */
        if(document.getElementById("keres_input") !== null){
            document.getElementById("keres_input").remove();
        }
        if(document.getElementById("sel_millio") !== null){
            document.getElementById("sel_millio").remove();
        }
        if(document.getElementById("sel_kot") !== null){
            document.getElementById("sel_kot").remove();
        }
        if(document.getElementById("sel_hat") !== null){
            document.getElementById("sel_hat").remove();
        }
        if(document.getElementById("sel_partner") !== null){
            document.getElementById("sel_partner").remove();
        }
        if(document.getElementById("sel_szerztip") !== null){
            document.getElementById("sel_szerztip").remove();
        }

        /* Hozzáadás */
        var sel = document.createElement("SELECT");
        sel.setAttribute("id","sel_irany");
        sel.setAttribute("class","form-control");
        sel.setAttribute("name","keres_text");
        document.getElementById("keres").appendChild(sel);

        /*Opt1*/
        var opt1 = document.createElement("OPTION");
        opt1.setAttribute("value","0");
        var opt1_szoveg = document.createTextNode("Bevétel");
        opt1.appendChild(opt1_szoveg);

        /*Opt2*/
        var opt2 = document.createElement("OPTION");
        opt2.setAttribute("value","1");
        var opt2_szoveg = document.createTextNode("Kiadás");
        opt2.appendChild(opt2_szoveg);

        /* SELECT elemhez való hozzáadás */
        document.getElementById("sel_irany").appendChild(opt1);
        document.getElementById("sel_irany").appendChild(opt2);


    }
    else if(val === "szerz_tipus"){

        /* Többi tag törlése */
        if(document.getElementById("keres_input") !== null){
            document.getElementById("keres_input").remove();
        }
        if(document.getElementById("sel_millio") !== null){
            document.getElementById("sel_millio").remove();
        }
        if(document.getElementById("sel_kot") !== null){
            document.getElementById("sel_kot").remove();
        }
        if(document.getElementById("sel_hat") !== null){
            document.getElementById("sel_hat").remove();
        }
        if(document.getElementById("sel_irany") !== null){
            document.getElementById("sel_irany").remove();
        }
        if(document.getElementById("sel_partner") !== null){
            document.getElementById("sel_partner").remove();
        }

        /* Hozzáadás */
        var sel_szerztip = document.createElement("SELECT");
        sel_szerztip.setAttribute("id","sel_szerztip");
        sel_szerztip.setAttribute("class","form-control");
        sel_szerztip.setAttribute("name","keres_text");
        document.getElementById("keres").appendChild(sel_szerztip);

        var nevek = ["Vállalkozói","Adás-vételi","Étkezési","Bérleti","Támogatási","Megbízási","Karbantartási","Közszolgálati","Tervezői","Vagyonkezelési","Megállapodás","Csereszerződés","Kötvény","Egyéb"];
        for(let i = 0; i<nevek.length;i++){
            /*Opt1*/
            opt_partner = document.createElement("OPTION");
            opt_partner.setAttribute("value",i);
            opt_partner_szoveg = document.createTextNode(nevek[i]);
            opt_partner.appendChild(opt_partner_szoveg);
            document.getElementById("sel_szerztip").appendChild(opt_partner);
        }
    }else if(val === "partner_tipus"){
        /* Többi tag törlése */
        if(document.getElementById("keres_input") !== null){
            document.getElementById("keres_input").remove();
        }
        if(document.getElementById("sel_millio") !== null){
            document.getElementById("sel_millio").remove();
        }
        if(document.getElementById("sel_kot") !== null){
            document.getElementById("sel_kot").remove();
        }
        if(document.getElementById("sel_hat") !== null){
            document.getElementById("sel_hat").remove();
        }
        if(document.getElementById("sel_partner") !== null){
            document.getElementById("sel_partner").remove();
        }
        if(document.getElementById("sel_szerztip") !== null){
            document.getElementById("sel_szerztip").remove();
        }

        /* Hozzáadás */
        var sel_partner = document.createElement("SELECT");
        sel_partner.setAttribute("id","sel_partner");
        sel_partner.setAttribute("class","form-control");
        sel_partner.setAttribute("name","keres_text");
        document.getElementById("keres").appendChild(sel_partner);

        /*Opt1*/
        var opt1_partner = document.createElement("OPTION");
        opt1_partner.setAttribute("value","0");
        var opt1_partner_szoveg = document.createTextNode("Vállalkozó");
        opt1_partner.appendChild(opt1_partner_szoveg);

        /*Opt2*/
        var opt2_partner = document.createElement("OPTION");
        opt2_partner.setAttribute("value","1");
        var opt2_partner_szoveg = document.createTextNode("Magánszemély");
        opt2_partner.appendChild(opt2_partner_szoveg);

        /* SELECT elemhez való hozzáadás */
        document.getElementById("sel_partner").appendChild(opt1_partner);
        document.getElementById("sel_partner").appendChild(opt2_partner);
    }
    else
        {
            /* Input mező létrehozása */
        if(!document.body.contains(document.getElementById("keres_input"))){
            var input_keres = document.createElement("INPUT");
            input_keres.setAttribute("type","text");
            input_keres.setAttribute("class","form-control");
            input_keres.setAttribute("id","keres_input");
            input_keres.setAttribute("name","keres_text");
            input_keres.setAttribute("placeholder","Keresés..");
            document.getElementById("keres").appendChild(input_keres);

            /* Többi tag törlése */
            if(document.getElementById("sel_millio") !== null){
                document.getElementById("sel_millio").remove();
            }
            if(document.getElementById("sel_kot") !== null){
                document.getElementById("sel_kot").remove();
            }
            if(document.getElementById("sel_hat") !== null){
                document.getElementById("sel_hat").remove();
            }
            if(document.getElementById("sel_irany") !== null){
                document.getElementById("sel_irany").remove();
            }
            if(document.getElementById("sel_partner") !== null){
                document.getElementById("sel_partner").remove();
            }
            if(document.getElementById("sel_szerztip") !== null){
                document.getElementById("sel_szerztip").remove();
            }
        }
        }
    }