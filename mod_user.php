<?php
require "functions/config.php";
include "templates/dash-head.php";
include "admin_auth.php";
$success = "";
$error = "";
if ($_SERVER["REQUEST_METHOD"] == "POST"){

    $userid = $_POST["id"];
    $username = $_POST["username"];
    $password = $_POST["password"];
    $password_hash = password_hash($password,PASSWORD_DEFAULT);
    $email = $_POST["email"];
    $name = $_POST["name"];
    $rights = $_POST["rights"];


    if(!empty($username) && !empty($password) && !empty($email) && !empty($name)){
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number = preg_match('@[0-9]@', $password);
        if(strlen($username) >= 6){
            if(strlen($password) >= 8 && $uppercase && $lowercase && $number){
                $conn = new mysqli(HOST, USER, PASS, DB);
                mysqli_set_charset($conn,"utf8");
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                $sql = "UPDATE users SET username = '$username', password = '$password_hash', email = '$email', rights='$rights' WHERE id = '$userid'";

                //if ($conn->query($sql) === TRUE) {
                if(mysqli_query($conn, $sql)){
                $success = "Sikerült a felhasználó módosítása!";
            }
                //}
                $conn->close();

            }else{
                $error = "A jelszó min. 8 karakter és tartalmaznia kell nagybetűt, kisbetűt, számot!";
            }
        }
        else{
            $error = "A felhasználónév min. 6 karakter!";
        }
    }
    elseif(!empty($username) && empty($password) && !empty($email) && !empty($name)){
        if(strlen($username) >= 6){
                $conn = new mysqli(HOST, USER, PASS, DB);
                mysqli_set_charset($conn,"utf8");
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                $sql = "UPDATE users SET username='$username', email='$email', rights='$rights' WHERE id = '$userid'";

                //if ($conn->query($sql) === TRUE) {
                    //$success = "Sikerült a felhasználó módosítása!";
                //}
            if(mysqli_query($conn, $sql)){
                $success = "Sikerült a felhasználó módosítása!";
            }
                $conn->close();
        }
        else{
            $error = "A felhasználónév min. 6 karakter!";
        }
    }
    else{
        $error = "valami";
    }
    }


?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Felhasználó módosítás</h1>

    </div>
    <?php
    if (!empty($success)) {
        echo "<div class='alert alert-success text-center role='alert'>";
        echo $success;
        echo "</div>";
    }
    if (!empty($error)) {
        echo "<div class='alert alert-danger text-center role='alert'>";
        echo $error;
        echo "</div>";
    }
    include "templates/dash-foot.php";
    ?>
