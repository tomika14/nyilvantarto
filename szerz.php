<?php
include ("templates/dash-head.php");
require_once ("functions/config.php");
$id = $_GET["id"];
$conn = new mysqli(HOST,USER,PASS,DB);
mysqli_set_charset($conn,"utf8");
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT szerzid,szerz_jel,partner_nev,szerzodes_targy,szerzodes_kelte,idoszak_kezd,idoszak_vege,lejar,netto,brutto,dijfiz_gyak,megjegyzes,hatarozott,millio,atlathato,asp_szerz,rovatrend,cofog,reszl_kod,userid,pdf,keszit_datum,name,partner_tipus,szerz_tipus,szerz_irany FROM szerzodesek INNER JOIN users ON szerzodesek.userid = users.id WHERE szerzid = '$id'";
$result = $conn->query($sql);
if (!$result) {
    trigger_error('Invalid query: ' . $conn->error);
}
if ($result->num_rows > 0) {
while($row = $result->fetch_assoc()) {

?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php echo "$row[szerzid]"; ?>. szerződés</h1>
    </div>
<form>
    <div class="form-group">
        <label for="szerzjel">Szerződést kötő jele</label>
        <input type="text" class="form-control" id="szerzjel" value="<?php
        if($row["szerz_jel"] == 0){
            echo "ÖNK";
        }
        else if($row["szerz_jel"] == 1){
            echo "HIV";
        }
        else if($row["szerz_jel"] == 2){
            echo "NNÖ";
        }
        ?>" readonly>
        <label for="szerz_tipus">Szerződés típusa</label>
        <input type="text" class="form-control" id="szerz_tipus" value="<?php
        switch ($row["szerz_tipus"]){
            case 0: echo "Vállalkozói"; break;
            case 1: echo "Adás-vételi"; break;
            case 2: echo "Étkezési"; break;
            case 3: echo "Bérleti"; break;
            case 4: echo "Támogatási"; break;
            case 5: echo "Megbízási"; break;
            case 6: echo "Karbantartási"; break;
            case 7: echo "Közszolgálati"; break;
            case 8: echo "Tervezői"; break;
            case 9: echo "Vagyonkezelési"; break;
            case 10: echo "Megállapodási"; break;
            case 11: echo "Csereszerződés"; break;
            case 12: echo "Kötvény"; break;
            case 13: echo "Egyéb"; break;
        }?>" readonly>
        <label for="szerz_irany">Szerződés iránya</label>
        <input type="text" class="form-control" id="szerz_irany" value="<?php
        switch ($row["szerz_irany"]){
            case 0: echo "Bevételi"; break;
            case 1: echo "Kiadás"; break;
        }?>" readonly>


        <label for="partner_nev">Partner neve</label>
        <input type="text" class="form-control" id="partner_nev" value="<?php echo "$row[partner_nev]"; ?>" readonly>
        <label for="partner_tipus">Partner típus</label>
        <input type="text" class="form-control" id="partner_tipus" value="<?php
        switch ($row["partner_tipus"]){
            case 0: echo "Vállalkozó"; break;
            case 1: echo "Magánszemély"; break;
        }?>" readonly>
        <label for="szerzodes_targy">Szerződés tárgya</label>
        <input type="text" class="form-control" id="szerzodes_targy" value="<?php echo "$row[szerzodes_targy]"; ?>" readonly>
        <label for="szerzodes_kelte">Szerződés kelte</label>
        <input type="text" class="form-control" id="szerzodes_kelte" value="<?php echo "$row[szerzodes_kelte]"; ?>" readonly>
        <label for="idoszak_kezd">Vonatkozó időszak kezdete</label>
        <input type="text" class="form-control" id="idoszak_kezd" value="<?php echo "$row[idoszak_kezd]"; ?>" readonly>
        <label for="idoszak_vege">Vonatkozó időszak kezdete</label>
        <input type="text" class="form-control" id="idoszak_vege" value="<?php echo "$row[idoszak_vege]"; ?>" readonly>
        <label for="idoszak_vege">Figyelmeztetés küldésének ideje</label>
        <input type="text" class="form-control" id="idoszak_vege" value="<?php echo "$row[lejar]"; ?>" readonly>
        <label for="netto">Nettó összeg</label>
        <input type="text" class="form-control" id="netto" value="<?php echo "$row[netto]"; ?>" readonly>
        <label for="brutto">Bruttó összeg</label>
        <input type="text" class="form-control" id="brutto" value="<?php echo "$row[brutto]"; ?>" readonly>
        <label for="dijfiz_gyak">Díjfizetés gyakorisága</label>
        <input type="text" class="form-control" id="dijfiz_gyak" value="<?php echo "$row[dijfiz_gyak]"; ?>" readonly>
        <div class="form-group">
            <label for="megjegyzes">Megjegyzés</label>
            <textarea class="form-control" id="megjegyzes" name="megjegyzes" rows="5" readonly><?php echo "$row[megjegyzes]"; ?>
            </textarea>
        </div>
        <label for="dijfiz_gyak">Határozott</label>
        <input type="text" class="form-control" id="dijfiz_gyak" value="<?php
        if($row["hatarozott"] == 0){
            echo "Határozott";
        }elseif($row["hatarozott"] == 1){
            echo "Határozatlan";
        } ?>" readonly><br />

        <label for="dijfiz_gyak">5 millió forintot elérő</label>
        <input type="text" class="form-control" id="dijfiz_gyak" value="<?php
        if($row["millio"] == 0){
            echo "Igen";
        }elseif($row["millio"] == 1){
            echo "Nem";
        } ?>" readonly><br />

        <label for="asp_szerz">ASP szerződés szám</label>
        <input type="text" class="form-control" id="asp_szerz" value="<?php echo "$row[asp_szerz]"; ?>" readonly>
        <label for="rovatrend">Rovatrend</label>
        <input type="text" class="form-control" id="rovatrend" value="<?php echo "$row[rovatrend]"; ?>" readonly>
        <label for="cofog">COFOG</label>
        <input type="text" class="form-control" id="cofog" value="<?php echo "$row[cofog]"; ?>" readonly>
        <label for="reszl_kod">Részletező kód</label>
        <input type="text" class="form-control" id="reszl_kod" value="<?php echo "$row[reszl_kod]"; ?>" readonly>
        <label for="keszit_datum">Felvitel ideje</label>
        <input type="text" class="form-control" id="keszit_datum" value="<?php echo "$row[keszit_datum]"; ?>" readonly>
        <label>Csatolt dokumentumok</label><br />
        <a href="upload/atlathatosagi/<?php echo "$row[atlathato]"; ?>">Átláthatósági nyilatkozat</a><br />
        <a href="upload/szerzodes/<?php echo "$row[pdf]"; ?>">Szerződés</a><br/>
    </div>
</form>

<?php
}
}
include ("templates/dash-foot.php");
?>
