<?php
include ("templates/dash-head.php");
include ("admin_auth.php");
require_once("functions/config.php");
$error = "";
$success = "";
$id = $_GET["id"];
$conn = new mysqli(HOST, USER, PASS, DB);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql_select = "SELECT id,username,name,email,rights FROM users WHERE id = '$id'";

$result3 = $conn->query($sql_select);

if ($result3->num_rows > 0){
while($row_data = $result3->fetch_assoc()) {
?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php echo $row_data["username"]; ?> felhasználó módosítása</h1>

    </div>
    <?php
    if (!empty($success)) {
        echo "<div class='alert alert-success text-center role='alert'>";
        echo $success;
        echo "</div>";
    }
    if (!empty($error)) {
        echo "<div class='alert alert-danger text-center role='alert'>";
        echo $error;
        echo "</div>";
    }
    ?>
    <form method="post" action="mod_user.php">
        <div class="form-group">
            <input type="hidden" id="id" name="id" value="<?php echo $id; ?>">
            <label for="username">Felhasználónév(min 6 kar.)</label>
            <input type="text" class="form-control" id="username" name="username" placeholder="Felhasználónév" value="<?php echo $row_data["username"]; ?>">
            <label for="password">Jelszó(min 8 kar.)</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Jelszó">
            <label for="name">Teljes név</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Teljes név" value="<?php echo $row_data["name"]; ?>">
            <label for="email">E-mail cím</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="E-mail cím" value="<?php echo $row_data["email"]; ?>">
            <label for="rights">Adminisztrátori beállítás</label>
            <?php
            if($row_data["rights"] == 0){
            echo '<select class="form-control" id="rights" name="rights">
                <option value="0" selected="selected">Felhasználó</option>
                <option value="1">Adminisztrátor</option>
                <option value="2">Nézelődő</option>
            </select>';
            }elseif ($row_data["rights"] == 1){
                echo '<select class="form-control" id="rights" name="rights">
                <option value="0">Felhasználó</option>
                <option value="1" selected="selected">Adminisztrátor</option>
                <option value="2">Nézelődő</option>
            </select>';
            }elseif ($row_data["rights"] == 2){
                echo '<select class="form-control" id="rights" name="rights">
                <option value="0">Felhasználó</option>
                <option value="1">Adminisztrátor</option>
                <option value="2" selected="selected">Nézelődő</option>
            </select>';
            }
            }
            }

            ?>
        </div>
        <input type="submit" class="btn btn-primary" value="Beküldés">
    </form>
    <?php
    include("templates/dash-foot.php");
    ?>
