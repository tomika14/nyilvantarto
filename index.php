<?php
$error = "";
require_once("functions/config.php");
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    session_start();
    $con = mysqli_connect(HOST, USER, PASS, DB);
    mysqli_set_charset($con,"utf8");
    if (mysqli_connect_errno()) {
        $error = 'Nem sikerült csatlakozni az adatbázishoz: ' . mysqli_connect_error();
    }
    if (!isset($_POST['username'], $_POST['password'])) {
        $error = 'Minden mezőt ki kell tölteni!';
    }
    if ($stmt = $con->prepare('SELECT id, password, rights, name, email FROM users WHERE username = ?')) {
        $stmt->bind_param('s', $_POST['username']);
        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            $stmt->bind_result($id, $password, $rights, $name, $email);
            $stmt->fetch();

            if (password_verify($_POST['password'], $password)) {
                session_regenerate_id();
                $_SESSION['loggedin'] = TRUE;
                $_SESSION['username'] = $_POST['username'];
                $_SESSION['id'] = $id;
                $_SESSION['rights'] = $rights;
                $_SESSION['name'] = $name;
                $_SESSION['email'] = $email;
                header("Location: dash.php");
            } else {
                $error = 'Hibás felhasználónév vagy jelszó!';
            }
        } else {
            $error = 'Hibás felhasználónév vagy jelszó!';
        }
        $stmt->close();
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>Bejelentkezés</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="js/showPassword.js"></script>
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <link href="css/signin.css" rel="stylesheet">
</head>
<body class="text-center">
<form class="form-signin" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <img src="img/logo.svg" id="icon" alt="Pilisvörösvár"/><br />
    <?php
    if (!empty($error)) {
        echo "<div class='alert alert-danger' role='alert'>";
        echo $error;
        echo "</div>";
    }
    ?>
    <label for="inputEmail" class="sr-only">Felhasználónév</label>
    <input type="text" id="username" class="form-control" placeholder="Felhasználónév" name="username" required autofocus>
    <label for="inputPassword" class="sr-only">Jelszó</label>
    <input type="password" id="password" class="form-control" placeholder="Jelszó" name="password" required>
    <label for="showpassword">Jelszó megjelenítése</label>
    <input type="checkbox" id="showpassword" onclick=Show()>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Bejelentkezés</button>

    <p class="mt-5 mb-3 text-muted">&copy; 2021</p>
</form>
</body>
</html>
