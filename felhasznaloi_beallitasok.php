<?php
include("templates/dash-head.php");
require_once("functions/config.php");
$error = "";
$success = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $username = $_POST["username"];
    $password = $_POST["password"];
    $password_hash = password_hash($password, PASSWORD_DEFAULT);
    $password_again = $_POST["password_again"];
    $email = $_POST["email"];
    $id = $_SESSION["id"];
    $name = $_POST["name"];
    $rights = $_POST["rights"];
    $uppercase = preg_match('@[A-Z]@', $password);
    $lowercase = preg_match('@[a-z]@', $password);
    $number = preg_match('@[0-9]@', $password);

    if (!empty($username) && !empty($password) && !empty($email) && !empty($name)) {
        if (strcmp($password, $password_again) == 0) {
            if (strlen($password) >= 8 && $uppercase && $lowercase && $number) {
                $conn = new mysqli(HOST, USER, PASS, DB);
                mysqli_set_charset($conn,"utf8");
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                $sql = "UPDATE users SET password = '$password_hash' WHERE id = '$id'";

                if ($conn->query($sql) === TRUE) {
                    $success = "Sikerült a felhasználó létrehozása!";
                }
                $conn->close();

            } else {
                $error = "A jelszó min. 8 karakter és tartalmaznia kell nagybetűt, kisbetűt, számot!";
            }
        } else {
            $error = "A két jelszó nem egyezik!";
        }
    } else {
        $error = "Mindent ki kell tölteni!";
    }

}

?>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Jelszó változtatás</h1>

    </div>
    <?php
    if (!empty($success)) {
        echo "<div class='alert alert-success text-center role='alert'>";
        echo $success;
        echo "</div>";
    }
    if (!empty($error)) {
        echo "<div class='alert alert-danger text-center role='alert'>";
        echo $error;
        echo "</div>";
    }
    ?>
    <script src="js/showPassword.js"></script>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" enctype="multipart/form-data">
        <div class="form-group">
            <p class="text-muted">
                Információ:
            </p>
            <p class="text-muted">
                Jelszó: minimum 8 karakter, kisbetű, nagybetű, szám<br/>
            </p>
            <label for="username">Felhasználónév</label>
            <input type="text" class="form-control" id="username" name="username"
                   value="<?php echo $_SESSION["username"]; ?>" placeholder="<?php echo $_SESSION["username"]; ?>>"
                   readonly>
            <label for="name">Teljes név</label>
            <input type="text" class="form-control" id="name" name="name" value="<?php echo $_SESSION["name"]; ?>"
                   placeholder="<?php echo $_SESSION["name"]; ?>" readonly>
            <label for="email">E-mail cím</label>
            <input type="email" class="form-control" id="email" name="email" value="<?php echo $_SESSION["email"]; ?>"
                   placeholder="<?php echo $_SESSION["email"]; ?>" readonly>
            <label for="password">Új Jelszó</label>
            <input type="password" class="form-control" id="password" name="password">
            <label for="password">Új jelszó újra</label>
            <input type="password" class="form-control" id="password" name="password_again">

        </div>
        <input type="submit" class="btn btn-primary" value="Beküldés">
    </form>
<?php

include("templates/dash-foot.php");
?>