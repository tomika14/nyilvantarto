<?php
include("functions/config.php");
include("templates/dash-head.php");
include ("user_auth.php");
$success = "";
$error = "";
if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_FILES["atlathato"]["name"]) && !empty($_FILES["szerzodes"]["name"])) {

    // Változók
    $atlathato_folder = "upload/atlathatosagi/";
    $atlathato_folder = $atlathato_folder . basename($_FILES['atlathato']['name']);
    $atlathato_filename = basename($_FILES['atlathato']['name']);
    $szerzodes_folder = "upload/szerzodes/";
    $szerzodes_folder = $szerzodes_folder . basename($_FILES['szerzodes']['name']);
    $szerzodes_filename = basename($_FILES['szerzodes']['name']);

    $szerz_jel = $_POST["szerz_jel"];
    $partner_nev = $_POST["partner_nev"];
    $partner_tipus = $_POST["partner_tipus"];
    $szerz_tipus = $_POST["szerz_tipus"];
    $szerz_irany = $_POST["szerz_irany"];
    $szerzodes_targy = $_POST["szerzodes_targy"];
    $szerzodes_kelte = $_POST["szerzodes_kelte"];
    $idoszak_kezd = $_POST["idoszak_kezd"];
    $idoszak_vege = $_POST["idoszak_vege"];
    $lejar = $_POST["lejar"];
    $netto = $_POST["netto"];
    $brutto = $_POST["brutto"];
    $dijfiz_gyak = $_POST["dijfiz_gyak"];
    $megjegyzes = $_POST["megjegyzes"];
    $hatarozott = $_POST["hatarozott"];
    $millio = $_POST["millio"];
    $atlathato = $atlathato_filename;
    $asp_szerz = $_POST["asp_szerz"];
    $rovatrend = $_POST["rovatrend"];
    $cofog = $_POST["cofog"];
    $reszl_kod = $_POST["reszl_kod"];
    $pdf = $szerzodes_filename;
    $userid = $_SESSION["id"];
    if (!empty($partner_nev) && !empty($szerzodes_targy) && !empty($szerzodes_kelte) && !empty($idoszak_kezd) && !empty($idoszak_vege) && !empty($netto) && !empty($brutto) && !empty($dijfiz_gyak) && !empty($megjegyzes) && !empty($atlathato) && !empty($pdf) && !empty($lejar)) {
        if (!empty($atlathato) && !empty($pdf))
        {
            if (move_uploaded_file($_FILES['atlathato']['tmp_name'], $atlathato_folder)) {
                if (move_uploaded_file($_FILES['szerzodes']['tmp_name'], $szerzodes_folder)) {
                    $conn = new mysqli(HOST, USER, PASS, DB);
                    mysqli_set_charset($conn, "utf8");
// Check connection
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    }

                    $sql = "INSERT INTO szerzodesek (szerz_jel,partner_nev,szerzodes_targy,szerzodes_kelte,idoszak_kezd,idoszak_vege,lejar,netto,brutto,dijfiz_gyak,megjegyzes,hatarozott,millio,atlathato,asp_szerz,rovatrend,cofog,reszl_kod,userid,pdf,partner_tipus,szerz_tipus,szerz_irany) VALUES ('$szerz_jel','$partner_nev','$szerzodes_targy','$szerzodes_kelte','$idoszak_kezd','$idoszak_vege','$lejar','$netto','$brutto','$dijfiz_gyak','$megjegyzes','$hatarozott','$millio','$atlathato','$asp_szerz','$rovatrend','$cofog','$reszl_kod','$userid','$pdf','$partner_tipus','$szerz_tipus','$szerz_irany')";
                    if (mysqli_query($conn, $sql)) {
                        //header("Location: ujszerzodes.php");
                        $success = "Sikeres feltöltés!";
                    } else {
                        echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
                    }
                }else{ $error = "Nem sikerült a fájlfeltöltés!";}
            } else { $error = "Nem sikerült a fájlfeltöltés!";}
        }
        elseif (!empty($atlathato) && empty($pdf))
        {
            if (move_uploaded_file($_FILES['atlathato']['tmp_name'], $atlathato_folder)) {
                    $conn = new mysqli(HOST, USER, PASS, DB);
                    mysqli_set_charset($conn, "utf8");
// Check connection
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    }

                    $sql = "INSERT INTO szerzodesek (szerz_jel,partner_nev,szerzodes_targy,szerzodes_kelte,idoszak_kezd,idoszak_vege,lejar,netto,brutto,dijfiz_gyak,megjegyzes,hatarozott,millio,atlathato,asp_szerz,rovatrend,cofog,reszl_kod,userid,partner_tipus,szerz_tipus,szerz_irany) VALUES ('$szerz_jel','$partner_nev','$szerzodes_targy','$szerzodes_kelte','$idoszak_kezd','$idoszak_vege','$lejar','$netto','$brutto','$dijfiz_gyak','$megjegyzes','$hatarozott','$millio','$atlathato','$asp_szerz','$rovatrend','$cofog','$reszl_kod','$userid','$partner_tipus','$szerz_tipus','$szerz_irany')";
                    if (mysqli_query($conn, $sql)) {
                        //header("Location: ujszerzodes.php");
                        $success = "Sikeres feltöltés!";
                    } else {
                        echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
                    }

            } else { $error = "Nem sikerült a fájlfeltöltés!";}
        }
        elseif(empty($atlathato) && !empty($pdf))
        {
                if (move_uploaded_file($_FILES['szerzodes']['tmp_name'], $szerzodes_folder)) {
                    $conn = new mysqli(HOST, USER, PASS, DB);
                    mysqli_set_charset($conn, "utf8");
// Check connection
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    }

                    $sql = "INSERT INTO szerzodesek (szerz_jel,partner_nev,szerzodes_targy,szerzodes_kelte,idoszak_kezd,idoszak_vege,lejar,netto,brutto,dijfiz_gyak,megjegyzes,hatarozott,millio,asp_szerz,rovatrend,cofog,reszl_kod,userid,pdf,partner_tipus,szerz_tipus,szerz_irany) VALUES ('$szerz_jel','$partner_nev','$szerzodes_targy','$szerzodes_kelte','$idoszak_kezd','$idoszak_vege','$lejar','$netto','$brutto','$dijfiz_gyak','$megjegyzes','$hatarozott','$millio','$asp_szerz','$rovatrend','$cofog','$reszl_kod','$userid','$pdf','$partner_tipus','$szerz_tipus','$szerz_irany')";
                    if (mysqli_query($conn, $sql)) {
                        //header("Location: ujszerzodes.php");
                        $success = "Sikeres feltöltés!";
                    } else {
                        echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
                    }
                }else{ $error = "Nem sikerült a fájlfeltöltés!";}


        }
        elseif(empty($atlathato) && empty($pdf))
        {
            $conn = new mysqli(HOST, USER, PASS, DB);
            mysqli_set_charset($conn, "utf8");
// Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "INSERT INTO szerzodesek (szerz_jel,partner_nev,szerzodes_targy,szerzodes_kelte,idoszak_kezd,idoszak_vege,lejar,netto,brutto,dijfiz_gyak,megjegyzes,hatarozott,millio,asp_szerz,rovatrend,cofog,reszl_kod,userid,partner_tipus,szerz_tipus,szerz_irany) VALUES ('$szerz_jel','$partner_nev','$szerzodes_targy','$szerzodes_kelte','$idoszak_kezd','$idoszak_vege','$lejar','$netto','$brutto','$dijfiz_gyak','$megjegyzes','$hatarozott','$millio','$asp_szerz','$rovatrend','$cofog','$reszl_kod','$userid','$partner_tipus','$szerz_tipus','$szerz_irany')";
            if (mysqli_query($conn, $sql)) {
                //header("Location: ujszerzodes.php");
                $success = "Sikeres feltöltés!";
            } else {
                echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
            }


        }


    }

}


/*szerzid,szerz_jel,partner_nev,szerzodes_targy,szerzodes_kelte,idoszak_kezd,idoszak_vege,netto,brutto,dijfiz_gyak,megjegyzes,hatarozott,atlathato,asp_szerz,rovatrend,cofog,reszl_kod,userid,pdf,keszit_datum
 */ ?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Új szerződés hozzáadása</h1>

    </div>
    <?php
    if (!empty($success)) {
        echo "<div class='alert alert-success text-center role='alert'>";
        echo $success;
        echo "</div>";
    }
    if (!empty($error)) {
        echo "<div class='alert alert-danger text-center role='alert'>";
        echo $error;
        echo "</div>";
    }
    ?>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" enctype="multipart/form-data">
        <div class="form-group">
            <label for="szerzjel">Szerződést kötő jele</label>
            <select class="form-control" id="szerz_jel" name="szerz_jel">
                <option value="0">ÖNK</option>
                <option value="1">HIV</option>
                <option value="2">NNÖ</option>
            </select>

            <label for="szerz_tipus">Szerződés típus</label>
            <select class="form-control" id="szerz_tipus" name="szerz_tipus">
                <option value="0">Vállalkozói</option>
                <option value="1">Adás-vételi</option>
                <option value="2">Étkezési</option>
                <option value="3">Bérleti</option>
                <option value="4">Támogatási</option>
                <option value="5">Megbízási</option>
                <option value="6">Karbantartási</option>
                <option value="7">Közszolgálati</option>
                <option value="8">Tervezői</option>
                <option value="9">Vagyonkezelési</option>
                <option value="10">Megállapodási</option>
                <option value="11">Csereszerződés</option>
                <option value="12">Kötvény</option>
                <option value="13">Egyéb</option>
            </select>

            <label for="szerz_irany">Szerződés iránya</label>
            <select class="form-control" id="szerz_irany" name="szerz_irany">
                <option value="0">Bevétel</option>
                <option value="1">Kiadás</option>
            </select>

            <label for="partner_nev">Partner neve</label>
            <input type="text" class="form-control" id="partner_nev" name="partner_nev" placeholder="Partner neve" required>
            <label for="partner_tipus">Partner típus</label>
            <select class="form-control" id="partner_tipus" name="partner_tipus">
                <option value="0">Vállalkozó</option>
                <option value="1">Magánszemély</option>
            </select>
            <label for="szerzodes_targy">Szerződés tárgya</label>
            <input type="text" class="form-control" id="szerzodes_targy" name="szerzodes_targy"
                   placeholder="Szerződés tárgya" required>
            <label for="szerzodes_kelte">Szerződés kelte</label>
            <input type="date" class="form-control" id="szerzodes_kelte" name="szerzodes_kelte" value="2017-06-01" required>
            <label for="idoszak_kezd">Vonatkozó időszak kezdete</label>
            <input type="date" class="form-control" id="idoszak_kezd" name="idoszak_kezd" value="2017-06-01" required>
            <label for="idoszak_vege">Vonatkozó időszak vége</label>
            <input type="date" class="form-control" id="idoszak_vege" name="idoszak_vege" value="2018-06-01" required>
            <label for="lejar">Figyelmeztetés küldésének dátuma</label>
            <input type="date" class="form-control" id="lejar" name="lejar" value="2018-06-01" required>
            <label for="netto">Nettó összeg(Ft.)</label>
            <input type="text" class="form-control" id="netto" name="netto" placeholder="Nettó" required>
            <label for="brutto">Bruttó összeg(Ft.)</label>
            <input type="text" class="form-control" id="brutto" name="brutto" placeholder="Bruttó" required>
            <label for="dijfiz_gyak">Díjfizetés gyakorisága</label>
            <input type="text" class="form-control" id="dijfiz_gyak" name="dijfiz_gyak"
                   placeholder="Díjfizetési gyakorisága" required>
            <div class="form-group">
                <label for="megjegyzes">Megjegyzés</label>
                <textarea class="form-control" id="megjegyzes" name="megjegyzes" rows="5" required></textarea>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="hatarozott" id="hatarozott" value="0"
                       checked>
                <label class="form-check-label" for="hatarozott">
                    Határozott
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="hatarozott" id="hatarozott2" value="1">
                <label class="form-check-label" for="hatarozott2">
                    Határozatlan
                </label>
            </div>
            <label class="form-check-label">
                5 millió forintot elérő, vagy meghaladó
            </label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="millio" id="millio" value="0"
                       checked>
                <label class="form-check-label" for="millio">
                    Igen
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="millio" id="millio2" value="1">
                <label class="form-check-label" for="millio2">
                    Nem
                </label>
            </div>
            <label for="asp_szerz">ASP szerződés szám</label>
            <input type="text" class="form-control" id="asp_szerz" name="asp_szerz" placeholder="ASP szerződés szám">
            <label for="rovatrend">Rovatrend</label>
            <input type="text" class="form-control" id="rovatrend" name="rovatrend" placeholder="Rovatrend">
            <label for="cofog">COFOG</label>
            <input type="text" class="form-control" id="cofog" name="cofog" placeholder="COFOG">
            <label for="reszl_kod">Részletező kód</label>
            <input type="text" class="form-control" id="reszl_kod" name="reszl_kod" placeholder="Részletező kód">
            <label for="atlathato">Átláthatósági nyilatkozat</label>
            <input type="file" class="form-control-file" id="atlathato" name="atlathato">
            <label for="szerzodes">Szerződés</label>
            <input type="file" class="form-control-file" id="szerzodes" name="szerzodes">


        </div>
        <input type="submit" class="btn btn-primary" value="Beküldés">
    </form>


    <?php
    include("templates/dash-foot.php");
    ?>
