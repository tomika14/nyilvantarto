<?php
include("templates/dash-head.php");
require_once("functions/config.php");
include ("user_auth.php");
$id = $_GET["id"];

$conn = new mysqli(HOST,USER,PASS,DB);
mysqli_set_charset($conn,"utf8");
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT szerzid,szerz_jel,partner_nev,szerzodes_targy,szerzodes_kelte,idoszak_kezd,idoszak_vege,lejar,netto,brutto,dijfiz_gyak,megjegyzes,hatarozott,millio,atlathato,asp_szerz,rovatrend,cofog,reszl_kod,userid,pdf,keszit_datum,name,partner_tipus,szerz_tipus,szerz_irany FROM szerzodesek INNER JOIN users ON szerzodesek.userid = users.id WHERE szerzid = '$id'";
$result = $conn->query($sql);
if (!$result) {
    trigger_error('Invalid query: ' . $conn->error);
}
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {

        ?>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2"><?php echo "$row[szerzid]"; ?>. szerződés</h1>
        </div>
        <?php
        if (!empty($success)) {
            echo "<div class='alert alert-success text-center role='alert'>";
            echo $success;
            echo "</div>";
        }
        if (!empty($error)) {
            echo "<div class='alert alert-danger text-center role='alert'>";
            echo $error;
            echo "</div>";
        }
        ?>
        <form method="post" action="mod_szerz.php" enctype="multipart/form-data">

            <input type="hidden" id="szerzid" name="szerzid" value="<?php echo "$row[szerzid]"; ?>">

            <div class="form-group">
                <label for="szerzjel">Szerződést kötő jele</label>
                <?php
                if ($row["szerz_jel"] == 0) {
                    echo "<select class=\"form-control\" id=\"szerz_jel\" name=\"szerz_jel\">
                    <option value=\"0\" selected='selected'>ÖNK</option>
                    <option value=\"1\">HIV</option>
                    <option value=\"2\">NNÖ</option>
                </select>";
                } else if ($row["szerz_jel"] == 1) {
                    echo "<select class=\"form-control\" id=\"szerz_jel\" name=\"szerz_jel\">
                    <option value=\"0\" >ÖNK</option>
                    <option value=\"1\" selected='selected'>HIV</option>
                    <option value=\"2\">NNÖ</option>
                </select>";
                } else if ($row["szerz_jel"] == 2) {
                    echo "<select class=\"form-control\" id=\"szerz_jel\" name=\"szerz_jel\">
                    <option value=\"0\">ÖNK</option>
                    <option value=\"1\">HIV</option>
                    <option value=\"2\" selected='selected'>NNÖ</option>
                </select>";
                }
                ?>
                <label for="szerz_tipus">Szerződés típusa</label>
                <?php
                switch ($row["szerz_tipus"]){
                    case 0:
                        echo "<select class=\"form-control\" id=\"szerz_tipus\" name=\"szerz_tipus\">
                                <option value=\"0\" selected='selected'>Vállalkozói</option>
                                <option value=\"1\">Adás-vételi</option>
                                <option value=\"2\">Étkezési</option>
                                <option value=\"3\">Bérleti</option>
                                <option value=\"4\">Támogatási</option>
                                <option value=\"5\">Megbízási</option>
                                <option value=\"6\">Karbantartási</option>
                                <option value=\"7\">Közszolgálati</option>
                                <option value=\"8\">Tervezői</option>
                                <option value=\"9\">Vagyonkezelési</option>
                                <option value=\"10\">Megállapodási</option>
                                <option value=\"11\">Csereszerződés</option>
                                <option value=\"12\">Kötvény</option>
                                <option value=\"13\">Egyéb</option>
                               </select>";
                        break;
                    case 1:                         echo "<select class=\"form-control\" id=\"szerz_tipus\" name=\"szerz_tipus\">
                                <option value=\"0\">Vállalkozói</option>
                                <option value=\"1\" selected='selected'>Adás-vételi</option>
                                <option value=\"2\">Étkezési</option>
                                <option value=\"3\">Bérleti</option>
                                <option value=\"4\">Támogatási</option>
                                <option value=\"5\">Megbízási</option>
                                <option value=\"6\">Karbantartási</option>
                                <option value=\"7\">Közszolgálati</option>
                                <option value=\"8\">Tervezői</option>
                                <option value=\"9\">Vagyonkezelési</option>
                                <option value=\"10\">Megállapodási</option>
                                <option value=\"11\">Csereszerződés</option>
                                <option value=\"12\">Kötvény</option>
                                <option value=\"13\">Egyéb</option>
                               </select>";
                        break;
                    case 2:                         echo "<select class=\"form-control\" id=\"szerz_tipus\" name=\"szerz_tipus\">
                                <option value=\"0\">Vállalkozói</option>
                                <option value=\"1\">Adás-vételi</option>
                                <option value=\"2\" selected='selected'>Étkezési</option>
                                <option value=\"3\">Bérleti</option>
                                <option value=\"4\">Támogatási</option>
                                <option value=\"5\">Megbízási</option>
                                <option value=\"6\">Karbantartási</option>
                                <option value=\"7\">Közszolgálati</option>
                                <option value=\"8\">Tervezői</option>
                                <option value=\"9\">Vagyonkezelési</option>
                                <option value=\"10\">Megállapodási</option>
                                <option value=\"11\">Csereszerződés</option>
                                <option value=\"12\">Kötvény</option>
                                <option value=\"13\">Egyéb</option>
                               </select>";
                        break;
                    case 3:                         echo "<select class=\"form-control\" id=\"szerz_tipus\" name=\"szerz_tipus\">
                                <option value=\"0\">Vállalkozói</option>
                                <option value=\"1\">Adás-vételi</option>
                                <option value=\"2\">Étkezési</option>
                                <option value=\"3\" selected='selected'>Bérleti</option>
                                <option value=\"4\">Támogatási</option>
                                <option value=\"5\">Megbízási</option>
                                <option value=\"6\">Karbantartási</option>
                                <option value=\"7\">Közszolgálati</option>
                                <option value=\"8\">Tervezői</option>
                                <option value=\"9\">Vagyonkezelési</option>
                                <option value=\"10\">Megállapodási</option>
                                <option value=\"11\">Csereszerződés</option>
                                <option value=\"12\">Kötvény</option>
                                <option value=\"13\">Egyéb</option>
                               </select>";
                        break;
                    case 4:                         echo "<select class=\"form-control\" id=\"szerz_tipus\" name=\"szerz_tipus\">
                                <option value=\"0\">Vállalkozói</option>
                                <option value=\"1\">Adás-vételi</option>
                                <option value=\"2\">Étkezési</option>
                                <option value=\"3\">Bérleti</option>
                                <option value=\"4\" selected='selected'>Támogatási</option>
                                <option value=\"5\">Megbízási</option>
                                <option value=\"6\">Karbantartási</option>
                                <option value=\"7\">Közszolgálati</option>
                                <option value=\"8\">Tervezői</option>
                                <option value=\"9\">Vagyonkezelési</option>
                                <option value=\"10\">Megállapodási</option>
                                <option value=\"11\">Csereszerződés</option>
                                <option value=\"12\">Kötvény</option>
                                <option value=\"13\">Egyéb</option>
                               </select>";
                        break;
                    case 5:                         echo "<select class=\"form-control\" id=\"szerz_tipus\" name=\"szerz_tipus\">
                                <option value=\"0\">Vállalkozói</option>
                                <option value=\"1\">Adás-vételi</option>
                                <option value=\"2\">Étkezési</option>
                                <option value=\"3\">Bérleti</option>
                                <option value=\"4\">Támogatási</option>
                                <option value=\"5\" selected='selected'>Megbízási</option>
                                <option value=\"6\">Karbantartási</option>
                                <option value=\"7\">Közszolgálati</option>
                                <option value=\"8\">Tervezői</option>
                                <option value=\"9\">Vagyonkezelési</option>
                                <option value=\"10\">Megállapodási</option>
                                <option value=\"11\">Csereszerződés</option>
                                <option value=\"12\">Kötvény</option>
                                <option value=\"13\">Egyéb</option>
                               </select>";
                        break;
                    case 6:                         echo "<select class=\"form-control\" id=\"szerz_tipus\" name=\"szerz_tipus\">
                                <option value=\"0\">Vállalkozói</option>
                                <option value=\"1\">Adás-vételi</option>
                                <option value=\"2\">Étkezési</option>
                                <option value=\"3\">Bérleti</option>
                                <option value=\"4\">Támogatási</option>
                                <option value=\"5\">Megbízási</option>
                                <option value=\"6\" selected='selected'>Karbantartási</option>
                                <option value=\"7\">Közszolgálati</option>
                                <option value=\"8\">Tervezői</option>
                                <option value=\"9\">Vagyonkezelési</option>
                                <option value=\"10\">Megállapodási</option>
                                <option value=\"11\">Csereszerződés</option>
                                <option value=\"12\">Kötvény</option>
                                <option value=\"13\">Egyéb</option>
                               </select>";
                        break;
                    case 7:                         echo "<select class=\"form-control\" id=\"szerz_tipus\" name=\"szerz_tipus\">
                                <option value=\"0\">Vállalkozói</option>
                                <option value=\"1\">Adás-vételi</option>
                                <option value=\"2\">Étkezési</option>
                                <option value=\"3\">Bérleti</option>
                                <option value=\"4\">Támogatási</option>
                                <option value=\"5\">Megbízási</option>
                                <option value=\"6\">Karbantartási</option>
                                <option value=\"7\" selected='selected'>Közszolgálati</option>
                                <option value=\"8\">Tervezői</option>
                                <option value=\"9\">Vagyonkezelési</option>
                                <option value=\"10\">Megállapodási</option>
                                <option value=\"11\">Csereszerződés</option>
                                <option value=\"12\">Kötvény</option>
                                <option value=\"13\">Egyéb</option>
                               </select>";
                        break;
                    case 8:                         echo "<select class=\"form-control\" id=\"szerz_tipus\" name=\"szerz_tipus\">
                                <option value=\"0\">Vállalkozói</option>
                                <option value=\"1\">Adás-vételi</option>
                                <option value=\"2\">Étkezési</option>
                                <option value=\"3\">Bérleti</option>
                                <option value=\"4\">Támogatási</option>
                                <option value=\"5\">Megbízási</option>
                                <option value=\"6\">Karbantartási</option>
                                <option value=\"7\">Közszolgálati</option>
                                <option value=\"8\" selected='selected'>Tervezői</option>
                                <option value=\"9\">Vagyonkezelési</option>
                                <option value=\"10\">Megállapodási</option>
                                <option value=\"11\">Csereszerződés</option>
                                <option value=\"12\">Kötvény</option>
                                <option value=\"13\">Egyéb</option>
                               </select>";
                        break;
                    case 9:                         echo "<select class=\"form-control\" id=\"szerz_tipus\" name=\"szerz_tipus\">
                                <option value=\"0\">Vállalkozói</option>
                                <option value=\"1\">Adás-vételi</option>
                                <option value=\"2\">Étkezési</option>
                                <option value=\"3\">Bérleti</option>
                                <option value=\"4\">Támogatási</option>
                                <option value=\"5\">Megbízási</option>
                                <option value=\"6\">Karbantartási</option>
                                <option value=\"7\">Közszolgálati</option>
                                <option value=\"8\">Tervezői</option>
                                <option value=\"9\" selected='selected'>Vagyonkezelési</option>
                                <option value=\"10\">Megállapodási</option>
                                <option value=\"11\">Csereszerződés</option>
                                <option value=\"12\">Kötvény</option>
                                <option value=\"13\">Egyéb</option>
                               </select>";
                        break;
                    case 10:                         echo "<select class=\"form-control\" id=\"szerz_tipus\" name=\"szerz_tipus\">
                                <option value=\"0\">Vállalkozói</option>
                                <option value=\"1\">Adás-vételi</option>
                                <option value=\"2\">Étkezési</option>
                                <option value=\"3\">Bérleti</option>
                                <option value=\"4\">Támogatási</option>
                                <option value=\"5\">Megbízási</option>
                                <option value=\"6\">Karbantartási</option>
                                <option value=\"7\">Közszolgálati</option>
                                <option value=\"8\">Tervezői</option>
                                <option value=\"9\" >Vagyonkezelési</option>
                                <option value=\"10\" selected='selected'>Megállapodási</option>
                                <option value=\"11\">Csereszerződés</option>
                                <option value=\"12\">Kötvény</option>
                                <option value=\"13\">Egyéb</option>
                               </select>";
                        break;
                    case 11:                         echo "<select class=\"form-control\" id=\"szerz_tipus\" name=\"szerz_tipus\">
                                <option value=\"0\">Vállalkozói</option>
                                <option value=\"1\">Adás-vételi</option>
                                <option value=\"2\">Étkezési</option>
                                <option value=\"3\">Bérleti</option>
                                <option value=\"4\">Támogatási</option>
                                <option value=\"5\">Megbízási</option>
                                <option value=\"6\">Karbantartási</option>
                                <option value=\"7\">Közszolgálati</option>
                                <option value=\"8\">Tervezői</option>
                                <option value=\"9\">Vagyonkezelési</option>
                                <option value=\"10\">Megállapodási</option>
                                <option value=\"11\" selected='selected'>Csereszerződés</option>
                                <option value=\"12\">Kötvény</option>
                                <option value=\"13\">Egyéb</option>
                               </select>";
                        break;
                    case 12:                         echo "<select class=\"form-control\" id=\"szerz_tipus\" name=\"szerz_tipus\">
                                <option value=\"0\">Vállalkozói</option>
                                <option value=\"1\">Adás-vételi</option>
                                <option value=\"2\">Étkezési</option>
                                <option value=\"3\">Bérleti</option>
                                <option value=\"4\">Támogatási</option>
                                <option value=\"5\">Megbízási</option>
                                <option value=\"6\">Karbantartási</option>
                                <option value=\"7\">Közszolgálati</option>
                                <option value=\"8\">Tervezői</option>
                                <option value=\"9\">Vagyonkezelési</option>
                                <option value=\"10\">Megállapodási</option>
                                <option value=\"11\">Csereszerződés</option>
                                <option value=\"12\" selected='selected'>Kötvény</option>
                                <option value=\"13\">Egyéb</option>
                               </select>";
                        break;
                    case 13:                         echo "<select class=\"form-control\" id=\"szerz_tipus\" name=\"szerz_tipus\">
                                <option value=\"0\">Vállalkozói</option>
                                <option value=\"1\">Adás-vételi</option>
                                <option value=\"2\">Étkezési</option>
                                <option value=\"3\">Bérleti</option>
                                <option value=\"4\">Támogatási</option>
                                <option value=\"5\">Megbízási</option>
                                <option value=\"6\">Karbantartási</option>
                                <option value=\"7\">Közszolgálati</option>
                                <option value=\"8\">Tervezői</option>
                                <option value=\"9\">Vagyonkezelési</option>
                                <option value=\"10\">Megállapodási</option>
                                <option value=\"11\">Csereszerződés</option>
                                <option value=\"12\">Kötvény</option>
                                <option value=\"13\" selected='selected'>Egyéb</option>
                               </select>";
                        break;
                }?>

                <label for="szerz_irany">Szerződés iránya</label>
                <?php
                if ($row["szerz_irany"] == 0) {
                    echo "<select class=\"form-control\" id=\"szerz_irany\" name=\"szerz_irany\">
                    <option value=\"0\" selected='selected'>Bevétel</option>
                    <option value=\"1\">Kiadás</option>
                </select>";
                } else if ($row["szerz_irany"] == 1) {
                    echo "<select class=\"form-control\" id=\"szerz_irany\" name=\"szerz_irany\">
                    <option value=\"0\" >Bevétel</option>
                    <option value=\"1\" selected='selected'>Kiadás</option>
                </select>";
                }
                ?>


                <label for="partner_nev">Partner neve</label>
                <input type="text" class="form-control" id="partner_nev" name="partner_nev" value="<?php echo "$row[partner_nev]"; ?>">

                <label for="partner_tipus">Partner típus</label>
                <?php
                if ($row["partner_tipus"] == 0) {
                    echo "<select class=\"form-control\" id=\"partner_tipus\" name=\"partner_tipus\">
                    <option value=\"0\" selected='selected'>Vállalkozó</option>
                    <option value=\"1\">Magánszemély</option>
                </select>";
                } else if ($row["partner_tipus"] == 1) {
                    echo "<select class=\"form-control\" id=\"partner_tipus\" name=\"partner_tipus\">
                    <option value=\"0\" >Vállalkozó</option>
                    <option value=\"1\" selected='selected'>Magánszemély</option>
                </select>";
                }
                ?>

                <label for="szerzodes_targy">Szerződés tárgya</label>
                <input type="text" class="form-control" id="szerzodes_targy" name="szerzodes_targy" value="<?php echo "$row[szerzodes_targy]"; ?>">
                <label for="szerzodes_kelte">Szerződés kelte</label>
                <input type="text" class="form-control" id="szerzodes_kelte" name="szerzodes_kelte" value="<?php echo "$row[szerzodes_kelte]"; ?>">
                <label for="idoszak_kezd">Vonatkozó időszak kezdete</label>
                <input type="text" class="form-control" id="idoszak_kezd" name="idoszak_kezd" value="<?php echo "$row[idoszak_kezd]"; ?>">
                <label for="idoszak_vege">Vonatkozó időszak vége</label>
                <input type="text" class="form-control" id="idoszak_vege" name="idoszak_vege" value="<?php echo "$row[idoszak_vege]"; ?>">
                <label for="idoszak_vege">Figyelmeztetés küldésének ideje</label>
                <input type="text" class="form-control" id="lejar" name="lejar" value="<?php echo "$row[lejar]"; ?>">
                <label for="netto">Nettó összeg</label>
                <input type="text" class="form-control" id="netto" name="netto" value="<?php echo "$row[netto]"; ?>">
                <label for="brutto">Bruttó összeg</label>
                <input type="text" class="form-control" id="brutto" name="brutto" value="<?php echo "$row[brutto]"; ?>">
                <label for="dijfiz_gyak">Díjfizetés gyakorisága</label>
                <input type="text" class="form-control" id="dijfiz_gyak" name="dijfiz_gyak" value="<?php echo "$row[dijfiz_gyak]"; ?>">
                <div class="form-group">
                    <label for="megjegyzes">Megjegyzés</label>
                    <textarea class="form-control" id="megjegyzes" name="megjegyzes"
                              rows="5"><?php echo "$row[megjegyzes]"; ?>
            </textarea>
                </div>
                <?php
                if ($row["hatarozott"] == 0) {
                    echo "<div class=\"form-check\">
        <input class=\"form-check-input\" type=\"radio\" name=\"hatarozott\" id=\"hatarozott\" value=\"0\" checked>
        <label class=\"form-check-label\" for=\"hatarozott\">Határozott</label>
        </div>
        <div class=\"form-check\">
        <input class=\"form-check-input\" type=\"radio\" name=\"hatarozott\" id=\"hatarozott2\" value=\"1\">
        <label class=\"form-check-label\" for=\"hatarozott2\">Határozatlan</label>
        </div>";
                } elseif ($row["hatarozott"] == 1) {
                    echo "<div class=\"form-check\">
        <input class=\"form-check-input\" type=\"radio\" name=\"hatarozott\" id=\"hatarozott\" value=\"0\">
        <label class=\"form-check-label\" for=\"hatarozott\">Határozott</label>
        </div>
        <div class=\"form-check\">
        <input class=\"form-check-input\" type=\"radio\" name=\"hatarozott\" id=\"hatarozott2\" value=\"1\" checked>
        <label class=\"form-check-label\" for=\"hatarozott2\">Határozatlan</label>
        </div>";
                } ?><br/>

                <label for="millio">5 millió forintot elérő</label>
                <?php
                if ($row["millio"] == 0) {
                    echo "<div class=\"form-check\">
        <input class=\"form-check-input\" type=\"radio\" name=\"millio\" id=\"millio\" value=\"0\" checked>
        <label class=\"form-check-label\" for=\"millio\">Igen</label>
        </div>
        <div class=\"form-check\">
        <input class=\"form-check-input\" type=\"radio\" name=\"millio\" id=\"millio2\" value=\"1\">
        <label class=\"form-check-label\" for=\"millio2\">Nem</label>
        </div>";
                } elseif ($row["millio"] == 1) {
                    echo "<div class=\"form-check\">
        <input class=\"form-check-input\" type=\"radio\" name=\"millio\" id=\"millio\" value=\"0\">
        <label class=\"form-check-label\" for=\"millio\">Igen</label>
        </div>
        <div class=\"form-check\">
        <input class=\"form-check-input\" type=\"radio\" name=\"millio\" id=\"millio2\" value=\"1\" checked>
        <label class=\"form-check-label\" for=\"millio2\">Nem</label>
        </div>";
                } ?><br/>
                <label for="asp_szerz">ASP szerződés szám</label>
                <input type="text" class="form-control" id="asp_szerz" name="asp_szerz" value="<?php echo "$row[asp_szerz]"; ?>">
                <label for="rovatrend">Rovatrend</label>
                <input type="text" class="form-control" id="rovatrend" name="rovatrend" value="<?php echo "$row[rovatrend]"; ?>">
                <label for="cofog">COFOG</label>
                <input type="text" class="form-control" id="cofog" name="cofog" value="<?php echo "$row[cofog]"; ?>">
                <label for="reszl_kod">Részletező kód</label>
                <input type="text" class="form-control" id="reszl_kod" name="reszl_kod" value="<?php echo "$row[reszl_kod]"; ?>">
                <label for="keszit_datum">Felvitel ideje</label>
                <input type="text" class="form-control" id="keszit_datum" name="keszit_datum" value="<?php echo "$row[keszit_datum]"; ?>">
                <label>Csatolt dokumentumok</label><br/>
                <a href="upload/atlathatosagi/<?php echo "$row[atlathato]"; ?>">Átláthatósági nyilatkozat</a><br/>
                <a href="upload/szerzodes/<?php echo "$row[pdf]"; ?>">Szerződés</a><br/>
                <label for="atlathato">Átláthatósági nyilatkozat</label>
                <input type="file" class="form-control-file" id="atlathato" name="atlathato">
                <label for="szerzodes">Szerződés</label>
                <input type="file" class="form-control-file" id="szerzodes" name="szerzodes">
                <input type="submit" class="btn btn-primary mt-3" value="Beküldés">
            </div>
        </form>

        <?php
    }
}
include("templates/dash-foot.php");
?>