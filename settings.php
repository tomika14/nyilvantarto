<?php
include ("templates/dash-head.php");
require_once("functions/config.php");
include ("admin_auth.php");

$error = "";
$success = "";
if ($_SERVER["REQUEST_METHOD"] == "POST"){

    $smtp_add = $_POST["smtp_add"];
    $smtp_port = $_POST["smtp_port"];
    $smtp_name = $_POST["smtp_name"];
    $smtp_pass = $_POST["smtp_pass"];
    $send_mail = $_POST["send_mail"];


    if(!empty($smtp_add) && !empty($smtp_port) && !empty($smtp_name) && !empty($smtp_pass)){

                $conn = new mysqli(HOST, USER, PASS, DB);
                mysqli_set_charset($conn,"utf8");
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }

        $sql_select = "SELECT smtp_add FROM settings";

        $result = $conn->query($sql_select);

        if ($result->num_rows > 0){
            while($row = $result->fetch_assoc()) {
                $tmp_add = $row["smtp_add"];
                $sql_up = "UPDATE settings SET smtp_add='$smtp_add', smtp_port='$smtp_port', smtp_name='$smtp_name', smtp_pass='$smtp_pass', send_mail='$send_mail' WHERE smtp_add = '$tmp_add'";
                if ($conn->query($sql_up) === TRUE){
                    $success = "Sikeres mentés!";
                }
                else{
                    $error = "Valami hiba történt!" . " " . $tmp_add . " " . $smtp_add . " " . $smtp_port . " " . $smtp_name . " " . $smtp_pass;
                }
            }
        }else {
            $sql = "INSERT INTO settings (smtp_add, smtp_port, smtp_name,smtp_pass,send_mail)
VALUES ('$smtp_add', '$smtp_port', '$smtp_name','$smtp_pass','$send_mail')";

            if ($conn->query($sql) === TRUE) {
                $success = "Új beállítás hozzáadva!";
            }
            $conn->close();
        }
    }
    else{
        $error = "Mindent ki kell tölteni!";
    }

}

?>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Beállítások</h1>

    </div>
    <?php
    if (!empty($success)) {
        echo "<div class='alert alert-success text-center role='alert'>";
        echo $success;
        echo "</div>";
    }
    if (!empty($error)) {
        echo "<div class='alert alert-danger text-center role='alert'>";
        echo $error;
        echo "</div>";
    }
    ?>
    <script src="js/showPassword.js"></script>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" enctype="multipart/form-data">
        <div class="form-group">
            <p class="text-muted">
                Információ:
            </p>
            <p class="text-muted">
                SMTP hoszt: pl. smtp.gmail.com<br />
                SMTP port: pl. 25<br />
                SMTP felhasználó: pl. teszt@gmail.com<br/>
                SMTP jelszó: pl. teszt123<br/>
            </p>
            <?php
            $conn = new mysqli(HOST, USER, PASS, DB);
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }

        $sql_select = "SELECT smtp_add,smtp_port,smtp_name,smtp_pass,send_mail FROM settings";

        $result3 = $conn->query($sql_select);

        if ($result3->num_rows > 0){
            while($row_data = $result3->fetch_assoc()) {
            ?>
            <label for="smtp_add">SMTP Hoszt:</label>
            <input type="text" class="form-control" id="smtp_add" name="smtp_add" placeholder="SMTP Hoszt" value="<?php echo $row_data["smtp_add"]; ?>">
            <label for="smtp_port">SMTP Port</label>
            <input type="text" class="form-control" id="smtp_port" name="smtp_port" placeholder="SMTP Port" value="<?php echo $row_data["smtp_port"]; ?>">
            <label for="smtp_name">SMTP Felhasználó</label>
            <input type="text" class="form-control" id="smtp_name" name="smtp_name" placeholder="SMTP Felhasználó" value="<?php echo $row_data["smtp_name"]; ?>">
            <label for="smtp_pass">SMTP Jelszó</label>
            <input type="password" class="form-control" id="password" name="smtp_pass" placeholder="SMTP Jelszó" value="<?php echo $row_data["smtp_pass"];?>">

            <label for="send_mail">Címzett e-mail címe:</label>
            <input type="text" class="form-control" id="send_mail" name="send_mail" placeholder="E-mail..." value="<?php echo $row_data["send_mail"];?>">

            <label for="showpassword">Jelszó megjelenítése</label>
            <input type="checkbox" id="showpassword" onclick=Show()>

        </div>
        <input type="submit" class="btn btn-primary" value="Mentés">
    </form>
<?php
}
        }else{

?>
        <label for="smtp_add">SMTP Hoszt:</label>
            <input type="text" class="form-control" id="smtp_add" name="smtp_add" placeholder="SMTP Hoszt">
            <label for="smtp_port">SMTP Port</label>
            <input type="text" class="form-control" id="smtp_port" name="smtp_port" placeholder="SMTP Port">
            <label for="smtp_name">SMTP Felhasználó</label>
            <input type="text" class="form-control" id="smtp_name" name="smtp_name" placeholder="SMTP Felhasználó">
            <label for="smtp_pass">SMTP Jelszó</label>
            <input type="password" class="form-control" id="password" name="smtp_pass" placeholder="SMTP Jelszó">

            <label for="send_mail">Címzett e-mail címe:</label>
            <input type="text" class="form-control" id="send_mail" name="send_mail" placeholder="E-mail...">

            <label for="showpassword">Jelszó megjelenítése</label>
            <input type="checkbox" id="showpassword" onclick=Show()>

        </div>
        <input type="submit" class="btn btn-primary" value="Mentés">
    </form>


<?php
}
include("templates/dash-foot.php");
?>