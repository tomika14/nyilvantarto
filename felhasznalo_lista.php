<?php
include("templates/dash-head.php");
include ("admin_auth.php");
require_once ("functions/config.php");
?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Felhasználói lista</h1>

    </div>
    <?php
    $conn = new mysqli(HOST,USER,PASS,DB);
    mysqli_set_charset($conn,"utf8");
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT id, username, email, name, rights, create_date FROM users";
    $result = $conn->query($sql);
    if (!$result) {
        trigger_error('Invalid query: ' . $conn->error);
    }
    if ($result->num_rows > 0) {
        echo "<div class='table-responsive'>";
        echo "<table class='table table-striped table-sm text-center'>";
        echo "<tr>";
        echo "<th>";
        echo "Azonosító:";
        echo "</th>";
        echo "<th>";
        echo "Felhasználónév:";
        echo "</th>";
        echo "<th>";
        echo "Email";
        echo "</th>";
        echo "<th>";
        echo "Név:";
        echo "</th>";
        echo "<th>";
        echo "Jogosultság:";
        echo "</th>";
        echo "<th>";
        echo "Létrehozás dátuma:";
        echo "</th>";
        echo "<th>";
        echo "Megtekintés";
        echo "</th>";
        echo "<th>";
        echo "Módosítás";
        echo "</th>";
        echo "<th>";
        echo "Törlés";
        echo "</th>";
        echo "</tr>";
        while($row = $result->fetch_assoc()) {

            echo "<tr>";
            echo "<td>";
            echo "$row[id]";
            echo "</td>";
            echo "<td>";
            echo "$row[username]";
            echo "</td>";
            echo "<td>";
            echo "$row[email]";
            echo "</td>";
            echo "<td>";
            echo "$row[name]";
            echo "</td>";
            echo "<td>";
            if($row["rights"] == 0){
                echo "Felhasználó";
            }elseif ($row["rights"] == 1){
                echo "Adminisztrátor";
            }
            elseif ($row["rights"] == 2){
                echo "Nézelődő";
            }
            echo "</td>";
            echo "<td>";
            echo "$row[create_date]";
            echo "</td>";
            echo "<td>";
            echo "<a href='user.php?id=$row[id]'>Megtekintés</a>";
            echo "</td>";
            echo "<td>";
            echo "<a href='moduser.php?id=$row[id]'>Módosítás</a>";
            echo "</td>";
            echo "<td>";
            echo "<a href='deluser.php?id=$row[id]' onclick='return confirm(\"Biztos, hogy törli?\")'>Törlés</a>";
            echo "</td>";
            echo "</tr>";


        }
        echo "</table>";
    } else {
        echo "Nincs szerződés!";
    }
    $conn->close();
    ?>
    <?php
include ("templates/dash-foot.php");
?>
