<?php
require "functions/config.php";
include "templates/dash-head.php";
include "user_auth.php";
$success = "";
$error = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Változók

    $atlathato_folder = "upload/atlathatosagi/";
    $atlathato_folder = $atlathato_folder . basename($_FILES['atlathato']['name']);
    $atlathato_filename = basename($_FILES['atlathato']['name']);
    $szerzodes_folder = "upload/szerzodes/";
    $szerzodes_folder = $szerzodes_folder . basename($_FILES['szerzodes']['name']);
    $szerzodes_filename = basename($_FILES['szerzodes']['name']);
    $id = $_POST["szerzid"];
    $szerz_jel = $_POST["szerz_jel"];
    $partner_tipus = $_POST["partner_tipus"];
    $szerz_tipus = $_POST["szerz_tipus"];
    $szerz_irany = $_POST["szerz_irany"];
    $partner_nev = $_POST["partner_nev"];
    $szerzodes_targy = $_POST["szerzodes_targy"];
    $szerzodes_kelte = $_POST["szerzodes_kelte"];
    $idoszak_kezd = $_POST["idoszak_kezd"];
    $idoszak_vege = $_POST["idoszak_vege"];
    $lejar = $_POST["lejar"];
    $netto = $_POST["netto"];
    $brutto = $_POST["brutto"];
    $dijfiz_gyak = $_POST["dijfiz_gyak"];
    $megjegyzes = $_POST["megjegyzes"];
    $hatarozott = $_POST["hatarozott"];
    $millio = $_POST["millio"];
    $atlathato = $atlathato_filename;
    $asp_szerz = $_POST["asp_szerz"];
    $rovatrend = $_POST["rovatrend"];
    $cofog = $_POST["cofog"];
    $reszl_kod = $_POST["reszl_kod"];
    $pdf = $szerzodes_filename;
    $userid = $_SESSION["id"];
    if (!empty($atlathato) && !empty($pdf)) {
        if (move_uploaded_file($_FILES['atlathato']['tmp_name'], $atlathato_folder)) {
            if (move_uploaded_file($_FILES['szerzodes']['tmp_name'], $szerzodes_folder)) {
                $conn = new mysqli(HOST, USER, PASS, DB);
                mysqli_set_charset($conn, "utf8");
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                    $sql_2 = "UPDATE szerzodesek SET szerz_jel = '$szerz_jel', partner_nev = '$partner_nev', szerzodes_targy = '$szerzodes_targy', szerzodes_kelte = '$szerzodes_kelte', idoszak_kezd = '$idoszak_kezd', idoszak_vege = '$idoszak_vege', lejar = '$lejar', netto = '$netto', brutto = '$brutto', dijfiz_gyak='$dijfiz_gyak', megjegyzes='$megjegyzes', hatarozott='$hatarozott', millio='$millio', atlathato='$atlathato', asp_szerz='$asp_szerz', rovatrend='$rovatrend', cofog='$cofog', reszl_kod='$reszl_kod', pdf='$pdf', partner_tipus='$partner_tipus',szerz_tipus='$szerz_tipus',szerz_irany='$szerz_irany' WHERE szerzid = '$id'";

                if (mysqli_query($conn, $sql_2)) {
                    //header("Location: ujszerzodes.php");
                    $success = "Sikeres módosítás!";
                } else {
                    echo "ERROR: Could not able to execute $sql_2. " . mysqli_error($conn);
                }
            } else {
                $error = "Nem sikerült a fájlfeltöltés! 2";
            }
        } else {
            $error = "Nem sikerült a fájlfeltöltés!";
        }
    }
    elseif (empty($pdf) && empty($atlathato)) {
        $conn = new mysqli(HOST, USER, PASS, DB);
        mysqli_set_charset($conn, "utf8");
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }


            $sql_2 = "UPDATE szerzodesek SET szerz_jel = '$szerz_jel', partner_nev = '$partner_nev', szerzodes_targy = '$szerzodes_targy', szerzodes_kelte = '$szerzodes_kelte', idoszak_kezd = '$idoszak_kezd', idoszak_vege = '$idoszak_vege', lejar = '$lejar', netto = '$netto', brutto = '$brutto', dijfiz_gyak='$dijfiz_gyak', megjegyzes='$megjegyzes', hatarozott='$hatarozott', millio='$millio', asp_szerz='$asp_szerz', rovatrend='$rovatrend', cofog='$cofog', reszl_kod='$reszl_kod', partner_tipus='$partner_tipus',szerz_tipus='$szerz_tipus',szerz_irany='$szerz_irany' WHERE szerzid = '$id'";

        if (mysqli_query($conn, $sql_2)) {
            //header("Location: ujszerzodes.php");
            $success = "Sikeres módosítás!";
        } else {
            echo "ERROR: Could not able to execute $sql_2. " . mysqli_error($conn);
        }

    }
    elseif (!empty($pdf) && empty($atlathato)){
        if (move_uploaded_file($_FILES['szerzodes']['tmp_name'], $szerzodes_folder)) {
            $conn = new mysqli(HOST, USER, PASS, DB);
            mysqli_set_charset($conn, "utf8");
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

                $sql_2 = "UPDATE szerzodesek SET szerz_jel = '$szerz_jel', partner_nev = '$partner_nev', szerzodes_targy = '$szerzodes_targy', szerzodes_kelte = '$szerzodes_kelte', idoszak_kezd = '$idoszak_kezd', idoszak_vege = '$idoszak_vege', lejar = '$lejar', netto = '$netto', brutto = '$brutto', dijfiz_gyak='$dijfiz_gyak', megjegyzes='$megjegyzes', hatarozott='$hatarozott', millio='$millio', asp_szerz='$asp_szerz', rovatrend='$rovatrend', cofog='$cofog', reszl_kod='$reszl_kod', pdf='$pdf', partner_tipus='$partner_tipus',szerz_tipus='$szerz_tipus',szerz_irany='$szerz_irany' WHERE szerzid = '$id'";

            if (mysqli_query($conn, $sql_2)) {
                $success = "Sikeres módosítás!";
            } else {
                echo "ERROR: Could not able to execute $sql_2. " . mysqli_error($conn);
            }
        } else {
            $error = "Nem sikerült a fájlfeltöltés! 2";
        }
    }
    elseif (!empty($atlathato) && empty($pdf)){
        if (move_uploaded_file($_FILES['atlathato']['tmp_name'], $atlathato_folder)) {

                $conn = new mysqli(HOST, USER, PASS, DB);
                mysqli_set_charset($conn, "utf8");
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }

                    $sql_2 = "UPDATE szerzodesek SET szerz_jel = '$szerz_jel', partner_nev = '$partner_nev', szerzodes_targy = '$szerzodes_targy', szerzodes_kelte = '$szerzodes_kelte', idoszak_kezd = '$idoszak_kezd', idoszak_vege = '$idoszak_vege', lejar = '$lejar', netto = '$netto', brutto = '$brutto', dijfiz_gyak='$dijfiz_gyak', megjegyzes='$megjegyzes', hatarozott='$hatarozott', millio='$millio', atlathato='$atlathato', asp_szerz='$asp_szerz', rovatrend='$rovatrend', cofog='$cofog', reszl_kod='$reszl_kod', partner_tipus='$partner_tipus',szerz_tipus='$szerz_tipus',szerz_irany='$szerz_irany' WHERE szerzid = '$id'";
                if (mysqli_query($conn, $sql_2)) {
                    $success = "Sikeres módosítás!";
                } else {
                    echo "ERROR: Could not able to execute $sql_2. " . mysqli_error($conn);
                }

        } else {
            $error = "Nem sikerült a fájlfeltöltés!";
        }
    }
}

?>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Beállítások</h1>

    </div>
<?php
if (!empty($success)) {
    echo "<div class='alert alert-success text-center role='alert'>";
    echo $success;
    echo "</div>";
}
if (!empty($error)) {
    echo "<div class='alert alert-danger text-center role='alert'>";
    echo $error;
    echo "</div>";
}
include "templates/dash-foot.php";
?>