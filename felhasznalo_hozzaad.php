<?php
include ("templates/dash-head.php");
include ("admin_auth.php");
require_once("functions/config.php");
$error = "";
$success = "";
if ($_SERVER["REQUEST_METHOD"] == "POST"){

    $username = $_POST["username"];
    $password = $_POST["password"];
    $password_hash = password_hash($password,PASSWORD_DEFAULT);
    $email = $_POST["email"];
    $name = $_POST["name"];
    $rights = $_POST["rights"];
    $uppercase = preg_match('@[A-Z]@', $password);
    $lowercase = preg_match('@[a-z]@', $password);
    $number = preg_match('@[0-9]@', $password);


    if(!empty($username) && !empty($password) && !empty($email) && !empty($name)){

        if(strlen($username) >= 6){
            if(strlen($password) >= 8 && $uppercase && $lowercase && $number){
        $conn = new mysqli(HOST, USER, PASS, DB);
                mysqli_set_charset($conn,"utf8");
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        $sql = "INSERT INTO users (username, password, email,name,rights)
VALUES ('$username', '$password_hash', '$email','$name','$rights')";

        if ($conn->query($sql) === TRUE) {
            $success = "Sikerült a felhasználó létrehozása!";
        }
        $conn->close();

        }else{
             $error = "A jelszó min. 8 karakter és tartalmaznia kell nagybetűt, kisbetűt, számot!";
            }
        }
        else{
            $error = "A felhasználónév min. 6 karakter!";
        }
    }
    else{

        $error = "Mindent ki kell tölteni!";
    }

}

?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Új felhasználó hozzáadása</h1>

    </div>
    <?php
    if (!empty($success)) {
        echo "<div class='alert alert-success text-center role='alert'>";
        echo $success;
        echo "</div>";
    }
    if (!empty($error)) {
        echo "<div class='alert alert-danger text-center role='alert'>";
        echo $error;
        echo "</div>";
    }
    ?>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" enctype="multipart/form-data">
    <div class="form-group">
        <p class="text-muted">
            Információ:
        </p>
        <p class="text-muted">
            Felhasználónév: minimum 6 karakter<br />
            Jelszó: minimum 8 karakter, kisbetű, nagybetű, szám<br />
        </p>
        <label for="username">Felhasználónév(min 6 kar.)</label>
        <input type="text" class="form-control" id="username" name="username" placeholder="Felhasználónév">
        <label for="password">Jelszó(min 8 kar.)</label>
        <input type="password" class="form-control" id="password" name="password" placeholder="Jelszó">
        <label for="name">Teljes név</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Teljes név">
        <label for="email">E-mail cím</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="E-mail cím">
        <label for="rights">Adminisztrátori beállítás</label>
        <select class="form-control" id="rights" name="rights">
            <option value="0">Felhasználó</option>
            <option value="1">Adminisztrátor</option>
            <option value="2">Nézelődő</option>
        </select>

    </div>
    <input type="submit" class="btn btn-primary" value="Beküldés">
</form>
<?php

include("templates/dash-foot.php");
?>
